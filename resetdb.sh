#!/bin/sh
tar -zxvf dump.tar.gz

mongo meteor --eval "printjson(db.dropDatabase())"
mongorestore --db meteor dump/meteor
