Router.configure({
  layoutTemplate: 'masterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound',
  waitOn() {
    this.subscribe('alert');
    this.subscribe('infoTable');
    this.subscribe('jsonFile');
    return [
      this.subscribe('tables'),
    ];
  },
  onAfterAction() {
    if (Meteor.isClient) {
      import { getScrollBarWidth } from '../client/utils/utils';

      // $('.hideScrollBar .sidebar')
      //   .css('width', `calc(100% + ${getScrollBarWidth()}px)`);

      $('#infoBox .close').show();
    }
  },
});

Router.route('/', {
  name: 'home',
  where: 'client',
  layoutTemplate: 'home',
  onAfterAction() {
    document.title = 'MantisTable';
  },
});

Router.route('changelog', {
  name: 'changelog',
  template: 'changelog',
  where: 'client',
  onAfterAction() {
    document.title = 'Changelog | MantisTable';
  },
});

// TABLES
Router.route('/tables', {
  name: 'tablesList',
  template: 'listTables',
  where: 'client',
  onAfterAction() {
    document.title = 'List tables | MantisTable';
  },
});

Router.route('/processAll', {
  name: 'process',
  template: 'process',
  where: 'client',
  onAfterAction() {
    document.title = 'Process List tables | MantisTable';
  },
});

Router.route('tables/create', {
  name: 'createTables',
  template: 'CreateTables',
  where: 'client',
  onAfterAction() {
    document.title = 'Upload a table | MantisTable';
  },
});

Router.route('/tables/:_id', {
  name: 'editTable',
  template: 'EditTable',
  where: 'client',
  waitOn() {
    return Meteor.subscribe('jsonFile', this.params._id);
  },
  data() {
    return Tables.findOne({ _id: this.params._id });
  },
  onStop() {
    import { closeRightSideBar, reRenderHandsontable } from '../client/utils/utils';

    closeRightSideBar();
  },
  onAfterAction() {
    import { reRenderHandsontable } from '../client/utils/utils';

    setTimeout(function () {
      reRenderHandsontable();
    }, 100);

    document.title = 'Edit table | MantisTable';
  },
});

Router.route('/process/starting-table/:_id', {
  name: 'startingTable',
  template: 'StartingTable',
  where: 'client',
  controller: 'ProcessTableController',
  onAfterAction() {
    document.title = 'Process a table | MantisTable';
  },
});

Router.route('/process/pre-processing/:_id', {
  name: 'preProcessing',
  template: 'PreProcessing',
  where: 'client',
  controller: 'ProcessTableController',
  onAfterAction() {
    document.title = 'Data Preparation  | MantisTable';
  },
});

Router.route('/process/columns-analysis/:_id', {
  name: 'columnsAnalysis',
  template: 'ColumnsAnalysis',
  where: 'client',
  controller: 'ProcessTableController',
  onAfterAction() {
    document.title = 'Column Analysis | MantisTable';
  },
});

Router.route('/process/concept-datatype-annotation/:_id', {
  name: 'conceptDatatypeAnnotation',
  template: 'ConceptDatatypeAnnotation',
  where: 'client',
  controller: 'ProcessTableController',
  onAfterAction() {
    document.title = 'Concept and datatype annotation | MantisTable';
  },
});

Router.route('/process/relationships-annotation/:_id', {
  name: 'relationshipsAnnotation',
  template: 'RelationshipsAnnotation',
  where: 'client',
  controller: 'ProcessTableController',
  onAfterAction() {
    document.title = 'Predicate annotation | MantisTable';
  },
});

Router.route('/process/entity-linking/:_id', {
  name: 'entityLinking',
  template: 'EntityLinking',
  where: 'client',
  controller: 'ProcessTableController',
  onAfterAction() {
    document.title = 'Entity Linking | MantisTable';
  },
});

Router.route('/process/edit-annotations/:_id', {
  name: 'editAnnotations',
  template: 'EditAnnotations',
  where: 'client',
  controller: 'ProcessTableController',
  onAfterAction() {
    document.title = 'Edit Annotations | MantisTable';
  },
});
