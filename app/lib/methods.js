/** ************************************************************************** */
/*  Client and Server Methods */
import { HTTP } from 'meteor/http';

/** ************************************************************************** */

Meteor.methods({
  libmethod_name() {
    if (this.isSimulation) {
      //   // do some client stuff while waiting for
      //   // result from server.
      //   return;
    }
  },
  'updateProcessStatus'(ident, process, status_new) {
    InfoTable.update(
      { _id: ident, 'statusProcess.routeName': process },
      {
        $set: { 'statusProcess.$.status': status_new },
      },
    );

    if (status_new === 'doing') {
      InfoTable.update(
        { _id: ident, 'statusProcess.routeName': process },
        {
          $set: {
            'statusProcess.$.start': new Date(),
            'statusProcess.$.end': new Date(),
          },
        },
      );
    }

    if (status_new === 'done') {
      InfoTable.update(
        { _id: ident, 'statusProcess.routeName': process },
        {
          $set: { 'statusProcess.$.end': new Date() },
        },
      );
    }

    Tables.update({ _id: ident },
      {
        $set: {
          lastEditDate: new Date(),
        },
      });
  },

  'updateGlobalStatus'(ident, status_new) {
    Tables.update({ _id: ident },
      {
        $set: {
          globalStatus: status_new,
        },
      });
  },

  'getProcessStatus'(ident) {
    return InfoTable.findOne({ _id: 'file526591_0_cols1_rows20' }, { _id: 0, statusProcess: 1 });
  },

  setInitialInfoTable(ident) {
    const JSONdata = JSON.parse(Jsonfile.findOne({ _id: ident }).data);
    const numRows = JSONdata.length;
    const numCols = Object.keys(JSONdata[0]).length;

    InfoTable.update({ _id: ident },
      {
        $set: {
          stats: { row: numRows, col: numCols },
          statusProcess: [
            { routeName: 'preProcessing', process: 'data preparation', status: 'todo' },
            { routeName: 'columnsAnalysis', process: 'columns analysis', status: 'todo' },
            { routeName: 'conceptDatatypeAnnotation', process: 'concept and datatype annotation', status: 'todo' },
            { routeName: 'relationshipsAnnotation', process: 'predicate annotation', status: 'todo' },
            { routeName: 'entityLinking', process: 'entity linking', status: 'todo' },
          ],
        },
      }, { upsert: true });

    Tables.update({ _id: ident },
      {
        $set: {
          globalStatus: 'todo',
        },
      });
  },

  updateNeAnn(ident, colIndex, newAnnotation) {
    InfoTable.update({ _id: ident, 'neCols.index': colIndex },
      { $set: { 'neCols.$.type': newAnnotation } });
  },

  updateLitAnn(ident, colIndex, newDatatype) {
    let newDatatypeUrl;

    switch (newDatatype) {
      case 'xsd:string':
        newDatatypeUrl = 'http://www.w3.org/2001/XMLSchema#string';
        break;
      case 'xsd:double':
        newDatatypeUrl = 'http://www.w3.org/2001/XMLSchema#double;';
        break;
      case 'xsd:float':
        newDatatypeUrl = 'http://www.w3.org/2001/XMLSchema#float';
        break;
      case 'xsd:integer':
        newDatatypeUrl = 'http://www.w3.org/2001/XMLSchema#integer';
        break;
      case 'xsd:boolean':
        newDatatypeUrl = 'http://www.w3.org/2001/XMLSchema#boolean';
        break;
      case 'xsd:anyURI':
        newDatatypeUrl = 'http://www.w3.org/2001/XMLSchema#anyURI';
        break;
      case 'xsd:date':
        newDatatypeUrl = 'http://www.w3.org/2001/XMLSchema#date';
        break;
      default:
        break;
    }

    InfoTable.update({ _id: ident, 'litCols.index': colIndex },
      {
        $set: {
          'litCols.$.dataType.datatype': newDatatype,
          'litCols.$.dataType.datatypeUrl': newDatatypeUrl,
        },
      });
  },

  updatePredicateAnn(ident, colIndex, newAnnotation) {
    InfoTable.update({ _id: ident, 'neCols.index': colIndex },
      { $set: { 'neCols.$.rel': newAnnotation } });
    InfoTable.update({ _id: ident, 'litCols.index': colIndex },
      { $set: { 'litCols.$.rel': newAnnotation } });
  },
});
