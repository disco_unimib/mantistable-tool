import { Mongo } from 'meteor/mongo';

Tables = new Mongo.Collection('tables');

if (Meteor.isServer) {
  Tables.allow({
    insert(userId, doc) {
      return true;
    },

    update(userId, doc, fieldNames, modifier) {
      return true;
    },

    remove(userId, doc) {
      return true;
    },
  });
}
