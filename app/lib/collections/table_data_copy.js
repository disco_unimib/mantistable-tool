TableDataCopy = new Mongo.Collection('table_data_copy');


if (Meteor.isServer) {
  TableDataCopy.allow({
    insert: function (userId, doc) {
      return true;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    }
  });
}
