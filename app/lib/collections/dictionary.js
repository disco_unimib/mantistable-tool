Dictionary = new Mongo.Collection('dictionary');


if (Meteor.isServer) {
  Dictionary.allow({
    insert: function (userId, doc) {
      return true;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    }
  });
}
