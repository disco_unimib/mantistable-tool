InfoTable = new Mongo.Collection('info_table');


if (Meteor.isServer) {
    InfoTable.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}


