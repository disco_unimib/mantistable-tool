Annotations = new Mongo.Collection('annotations');


if (Meteor.isServer) {
  Annotations.allow({
    insert: function (userId, doc) {
      return true;
    },

    update: function (userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function (userId, doc) {
      return true;
    }
  });

}
