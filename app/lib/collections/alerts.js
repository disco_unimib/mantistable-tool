import SimpleSchema from 'simpl-schema';

Alerts = new Mongo.Collection('alerts');

if (Meteor.isServer) {
  Alerts.allow({
    insert(userId, doc) {
      return true;
    },
    update(userId, doc, fieldNames, modifier) {
      return true;
    },
    remove(userId, doc) {
      return true;
    },
  });
}
