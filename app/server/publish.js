Meteor.publish('tables', function () {
  return Tables.find({}); // you collection global variable
});

Meteor.publish('infoTable', function () {
  return InfoTable.find({}); // you collection global variable
});

Meteor.publish('infoTableByTable', function () {
  return InfoTable.find({ _id: this._params[0] }); // you collection global variable
});

Meteor.publish('jsonFile', function () {
  return Jsonfile.find({ _id: this._params[0] }); // you collection global variable
});

Meteor.publish('alert', function () {
  return Alerts.find(); // you collection global variable
});

Meteor.publish('tableDataCopy', function () {
  return TableDataCopy.find({ _id: this._params[0] }); // you collection global variable
});

Meteor.publish('annotations', function () {
  return Annotations.find({ tableID: this._params[0] }); // you collection global variable
});

Meteor.publish('dictionary', function () {
  return Dictionary.find(); // you collection global variable
});

Meteor.publish('statusProcess', function () {
  return InfoTable.find({}, { statusProcess: 1 }); // you collection global variable
});
