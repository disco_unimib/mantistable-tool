Router.route('downloadMyAnnotations', {
  name: 'download',
  where: 'server',
  subscriptions() {
    this.subscribe('infoTable');
  },
  action() {
    const json = InfoTable.find({ 'statusProcess.4.status': 'done', typeGS: { $ne: 'none' } }, {
      _id: 1,
      typeGS: 1,
      neCols: 1,
      noAnnCols: 1,
      subCol: 1,
      litCols: 1,
    })
      .fetch();
    const headers = {
      'Content-Type': 'text/json',
      'Content-Disposition': 'attachment; filename=myAnnotations.json',
    };
    this.response.writeHead(200, headers);
    return this.response.end(JSON.stringify(json, null, 2));
  },
});

Router.route('downloadPreview', {
  name: 'downloadPrev',
  where: 'server',
  action() {
    const headers = {
      'Content-Type': 'text/json',
      'Content-Disposition': 'attachment; filename=MantisTablePreview.json',
    };
    this.response.writeHead(200, headers);
    return this.response.end(Assets.getText('previewAnnotations.json'));
  },
});

Router.route('downloadForChallenge/CTA', {
  name: 'challengeCTATask',
  where: 'server',
  action() {
    const json = InfoTable.find({ 'statusProcess.4.status': 'done', typeGS: 'CTAround1' }, {
      _id: 1,
      stats: 0,
      statusProcess: 0,
      createdAt: 0,
      numCols: 0,
      numRows: 0,
      subCol: 0,
    });
    let text = '';
    json.forEach(function (item) {
      if (item.neCols) {
        (item.neCols).forEach(function (value) {
          if (value && value.type) {
            text += `"${item._id}",`;
            text += `"${value.index}",`;
            text += `"${value.type}"`;
            text += '\n';
          }
        });
      }

      if (item.litCols) {
        (item.litCols).forEach(function (value) {
          if (value && value.dataType.datatypeUrl) {
            text += `"${item._id}",`;
            text += `"${value.index}",`;
            text += `"${value.dataType.datatypeUrl}"`;
            text += '\n';
          }
        });
      }
    });

    const headers = {
      'Content-Type': 'text/csv',
      'Content-Disposition': 'attachment; filename=CTARound1.csv',
    };
    this.response.writeHead(200, headers);
    return this.response.end(text);
  },
});

Router.route('downloadForChallenge/CPA', {
  name: 'challengeCPATask',
  where: 'server',
  action() {
    const json = InfoTable.find({ 'statusProcess.4.status': 'done', typeGS: 'CPAround1' }, {
      _id: 1,
      stats: 0,
      statusProcess: 0,
      createdAt: 0,
      numCols: 0,
      numRows: 0,
      subCol: 0,
    });
    let text = '';
    json.forEach(function (item) {
      if (item.neCols || item.litCols) {
        (item.neCols.concat(item.litCols)).forEach(function (value) {
          if (value && value.rel) {
            text += `"${item._id}",`;
            text += `"${item.subCol}",`;
            text += `"${value.index}",`;
            text += `"${value.rel}"`;
            text += '\n';
          }
        });
      }
    });

    const headers = {
      'Content-Type': 'text/csv',
      'Content-Disposition': 'attachment; filename=CPARound1.csv',
    };
    this.response.writeHead(200, headers);
    return this.response.end(text);
  },
});

Router.route('downloadForChallenge/CEA', {
  name: 'challengeCEATask',
  where: 'server',
  action() {
    const json = TableDataCopy.find({ typeGS: 'CEAround1' });

    let text = '';
    // iterate over collection
    json.forEach(function (item) {

      // console.log(item);
      if (item.cols) {
        // iterate over each column
        let colIndex = 0;
        (item.cols).forEach(function (col) {
          // iterate over each cell per row
          let rowIndex = 0;
          col.forEach(function (cell) {
            if (cell && cell.linked_entity) {
              text += `"${item._id}",`;
              text += `"${colIndex}",`;
              text += `"${rowIndex}",`;
              text += `"${cell.linked_entity}"`;
              text += '\n';
            }
            rowIndex++;
          });

          colIndex++;
        });
      }
    });
    const headers = {
      'Content-Type': 'text/csv',
      'Content-Disposition': 'attachment; filename=CEARound1.csv',
    };
    this.response.writeHead(200, headers);
    return this.response.end(text);
  },
});
