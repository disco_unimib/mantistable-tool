Meteor.startup(() => {

  // reset global status of table with stuck step 0
  InfoTable.find({ 'statusProcess.status': 'doing' })
    .forEach(function (currentDoc) {
      if (currentDoc.statusProcess[0].status === 'doing') {
        Meteor.call('updateGlobalStatus', currentDoc._id, 'todo');
      }
    });
  // unstuck "doing" process if any
  InfoTable.update({ 'statusProcess.status': 'doing' },
    { $set: { 'statusProcess.$.status': 'todo' } }, { multi: true });

  Alerts.remove();
});
