import winston from 'winston/lib/winston';
import normalize from './ti-process/0-data-preparation/1.1-normalization/normalize';
import converter from './ti-process/0-data-preparation/1.1-normalization/converter';
import setInfoTable from './ti-process/1-columns-analysis/2.1-columns-classification/info-tables';
import getSubCol from './ti-process/1-columns-analysis/2.2-subject-column-detection/sub-detection';
import getEntities from './ti-process/2-concept-datatype-annotation/entities';
import {
  delTables, loadTables
} from './ti-process/utils/helpers';
import { getNErelationships } from './ti-process/3-predicate-annotation/relationships';
import getEntityLinking from './ti-process/4-entity-linking/linking';

const fs = require('fs');
const conv = converter();
const currency = fs.readFileSync('assets/app/currency.txt').toString().split('\r\n');
const iata = fs.readFileSync('assets/app/iatacodes.txt').toString().split('\r\n');
const table = Tables.find().fetch();
const challenge = false;
const debug = true;

Meteor.methods({
  'file-upload'(fileInfo, fileData, nameinput, typeGS) {
    Alerts.remove({});
    Alerts.insert({ state: 'uploadTable', message: 'Upload process started' });
    Alerts.insert({ state: 'uploadTable', message: `File name: ${fileInfo}` });
    Alerts.insert({ state: 'uploadTable', message: `Table name: ${nameinput}` });

    const original_id = new Mongo.ObjectID('')._str;
    Alerts.insert({ state: 'uploadTable', message: `Auto-generated id for the table: ${original_id}` });

    Tables.insert({
      _id: original_id,
      name: nameinput,
      file_name: fileInfo,
      date: new Date(),
      typeGS,
    });

    Jsonfile.insert({
      _id: original_id,
      data: fileData,
    });

    InfoTable.insert({
      _id: original_id,
      typeGS,
    });

    Meteor.call('setInitialInfoTable', original_id);

    Alerts.insert({ state: 'uploadTable', message: 'Upload process finished' });

    return true;
  },
  'file-delete'(fileid) {
    Tables.remove({ _id: fileid });
    Jsonfile.remove({ _id: fileid });
    InfoTable.remove({ _id: fileid });

    return true;
  },

  'deleteAllTables'() {
    Tables.remove({ });
    Jsonfile.remove({ });
    InfoTable.remove({ });

    return true;
  },

  loadTables(typeGS) {
    if(Tables.find({typeGS: typeGS}).count() == 0)
      loadTables(typeGS);
  },

  deleteTables(typeGS) {
    delTables(typeGS);
  },

  simpleUpdate(nameinput, ident) {
    Tables.update({ _id: ident },
      {
        $set: {
          name: nameinput,
          lastEditDate: new Date(),
        },
      });

    return true;
  },

  updateJson(fileInfo, fileData, nameinput, ident) {
    Tables.update({ _id: ident },
      {
        $set: {
          name: nameinput,
          file_name: fileInfo,
          lastEditDate: new Date(),
        },
      });

    Jsonfile.update({ _id: ident },
      {
        $set: {
          data: fileData,
        },
      });

    return true;
  },
  processJson(fileId) {
    normalize(fileId, conv, currency, iata);
    setInfoTable(fileId);
    getSubCol(fileId);
    getEntities(fileId);
    getNErelationships(fileId);
    //getLITrelationships(fileId);
  },
  preProcessing(fileId) {
    Meteor.defer(() => {
      try {
        Meteor.call('updateGlobalStatus', fileId, 'doing');
        Alerts.remove({ tableID: fileId });

        winston.log('silly', '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        winston.log('silly', 'PRE PROCESSING STARTED');

        normalize(fileId, conv, currency, iata);

        winston.log('silly', 'PRE PROCESSING ENDED');

        Meteor.call('updateProcessStatus', fileId, 'preProcessing', 'done');

        return true;
      } catch (e) {
        Meteor.call('updateProcessStatus', fileId, 'preProcessing', 'todo');
        Meteor.call('updateGlobalStatus', fileId, 'todo');
        console.log(e);
        return false;
      }
    });
  },

  columnsAnalysis(fileId) {
    Meteor.defer(() => {
      try {
        Alerts.remove({ tableID: fileId });

        winston.log('silly', '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        winston.log('silly', 'COLUMNS ANALYSIS STARTED');

        setInfoTable(fileId);
        getSubCol(fileId);

        winston.log('silly', 'COLUMNS ANALYSIS ENDED');

        Meteor.call('updateProcessStatus', fileId, 'columnsAnalysis', 'done');

        return true;
      } catch (e) {
        console.log(e);
        Meteor.call('updateProcessStatus', fileId, 'columnsAnalysis', 'todo');
        return false;
      }
    });
  },

  conceptDatatypeAnnotation(fileId) {
    Meteor.defer(() => {
      try {
        Alerts.remove({ tableID: fileId });

        winston.log('silly', '%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
        winston.log('silly', 'CONCEPT AND DATATYPE ANNOTATION STARTED');
       
        getEntities(fileId, debug, challenge);

        winston.log('silly', 'CONCEPT AND DATATYPE ANNOTATION ENDED');

        Meteor.call('updateProcessStatus', fileId, 'conceptDatatypeAnnotation', 'done');

        return true;
      } catch (e) {
        console.log(e);
        Meteor.call('updateProcessStatus', fileId, 'conceptDatatypeAnnotation', 'todo');
        return false;
      }
    });
  },

  relationshipsAnnotation(fileId) {
    Meteor.defer(() => {
      try {
        Alerts.remove({ tableID: fileId });
        getNErelationships(fileId);
        Meteor.call('updateProcessStatus', fileId, 'relationshipsAnnotation', 'done');
        return true;
      } catch (e) {
        console.log(e);
        Meteor.call('updateProcessStatus', fileId, 'relationshipsAnnotation', 'todo');
        return false;
      }
    });
  },

  entityLinking(fileId) {
    Meteor.defer(() => {
      try {
        Alerts.remove({ tableID: fileId });
        // do something....
        getEntityLinking(fileId);
        Meteor.call('updateProcessStatus', fileId, 'entityLinking', 'done');
        Meteor.call('updateGlobalStatus', fileId, 'done');
        return true;
      } catch (e) {
        Meteor.call('updateProcessStatus', fileId, 'entityLinking', 'todo');
        Meteor.call('updateGlobalStatus', fileId, 'doing');
        console.log(e);
        return false;
      }
    });
  },

  method1() {
    Meteor.defer(() => {
      for (let i = 0; i < table.length; i++) {
        if (i % 4 == 0) {
          const fileId = table[i]._id;
          process(fileId);
        }
      }
    });
  },

  method2() {
    Meteor.defer(() => {
      for (let i = 0; i < table.length; i++) {
        if (i % 4 == 1) {
          const fileId = table[i]._id;
          process(fileId);
        }
      }
    });
  },

  method3() {
    Meteor.defer(() => {
      for (let i = 0; i < table.length; i++) {
        if (i % 4 == 2) {
          const fileId = table[i]._id;
          process(fileId);
        }
      }
    });
  },

  method4() {
    Meteor.defer(() => {
      for (let i = 0; i < table.length; i++) {
        if (i % 4 == 3) {
          const fileId = table[i]._id;
          process(fileId);
        }
      }
    });
  },

  startAnnot() {
    Meteor.defer(() => {
      const table = Tables.find().fetch();
    
      for (let i = 0; i < 2; i++) {
        let fileId = table[i]._id;
        console.log(fileId);
        console.log("table: "+(i+1));
        try {
          Meteor.call('updateProcessStatus', fileId, 'preProcessing', 'doing');
          normalize(fileId, conv, currency, iata);
          setInfoTable(fileId);
          Meteor.call('updateProcessStatus', fileId, 'preProcessing', 'done');
        } catch (e) {
          console.log(e);
          Meteor.call('updateProcessStatus', fileId, 'preProcessing', 'todo');
          Meteor.call('updateGlobalStatus', fileId, 'todo');
          Log.insert({tableID:fileId, error: e.toString()});
        }
        try {
          Meteor.call('updateProcessStatus', fileId, 'columnsAnalysis', 'doing');
          getSubCol(fileId);
          Meteor.call('updateProcessStatus', fileId, 'columnsAnalysis', 'done');
        } catch (e) {
          console.log(e);
          Meteor.call('updateProcessStatus', fileId, 'columnsAnalysis', 'todo');
          Log.insert({tableID:fileId, error: e.toString()});
        }
            // concept and datatype annotation
        try {
          Meteor.call('updateProcessStatus', fileId, 'conceptDatatypeAnnotation', 'doing');
          getEntities(fileId, challenge);
          Meteor.call('updateProcessStatus', fileId, 'conceptDatatypeAnnotation', 'done');
        } catch (e) {
          console.log(e);
          Meteor.call('updateProcessStatus', fileId, 'conceptDatatypeAnnotation', 'todo');
          Log.insert({tableID:fileId, error: e.toString()});
        }
      }

    });
  },

});

function process(fileId) {
  // console.log(`*******************START ANNOTATION***************** FILE:${fileId}`);
  console.log("process: "+fileId);  
  // const stat = InfoTable.findOne({ _id: fileId }).statusProcess;
  const globalStatus = Tables.findOne({ _id: fileId }).globalStatus;

  if (globalStatus === 'todo') { // globalStatus !== 'done'
    Meteor.call('updateGlobalStatus', fileId, 'doing');
    // data preparation
    // if (stat[0].status === 'todo') {
    try {
      Meteor.call('updateProcessStatus', fileId, 'preProcessing', 'doing');
      normalize(fileId, conv, currency, iata);
      setInfoTable(fileId);
      Meteor.call('updateProcessStatus', fileId, 'preProcessing', 'done');
    } 
    catch (e) {
      Meteor.call('updateProcessStatus', fileId, 'preProcessing', 'todo');
      Meteor.call('updateGlobalStatus', fileId, 'todo');
      Log.insert({tableID: fileId, method: 'transformer', error: e.toString()});
    }
    // }

    // if (stat[1].status === 'todo') {
    // column analysis
    try {
      Meteor.call('updateProcessStatus', fileId, 'columnsAnalysis', 'doing');
      getSubCol(fileId);
      Meteor.call('updateProcessStatus', fileId, 'columnsAnalysis', 'done');
    } 
    catch (e) {
      Meteor.call('updateProcessStatus', fileId, 'columnsAnalysis', 'todo');
      Log.insert({tableID: fileId, method: 'transformer', error: e.toString()}); 
    }
    // }

    // concept and datatype annotation
    try {
      Meteor.call('updateProcessStatus', fileId, 'conceptDatatypeAnnotation', 'doing');
      getEntities(fileId, debug, challenge);
      Meteor.call('updateProcessStatus', fileId, 'conceptDatatypeAnnotation', 'done');
    } 
    catch (e) {
      Meteor.call('updateProcessStatus', fileId, 'conceptDatatypeAnnotation', 'todo');
      Log.insert({tableID: fileId, method: 'transformer', error: e.toString()}); 
    }

    /* // predicate annotation
    try {
      Meteor.call('updateProcessStatus', fileId, 'relationshipsAnnotation', 'doing');
      getNErelationships(fileId);
      //getLITrelationships(fileId);
      Meteor.call('updateProcessStatus', fileId, 'relationshipsAnnotation', 'done');
    } catch (e) {
      console.log(e);
      Meteor.call('updateProcessStatus', fileId, 'relationshipsAnnotation', 'todo');
      return;
    }

    try {
      Meteor.call('updateProcessStatus', fileId, 'entityLinking', 'doing');
      getEntityLinking(fileId);
      Meteor.call('updateProcessStatus', fileId, 'entityLinking', 'done');
    } catch (e) {
      console.log(e);
      Meteor.call('updateProcessStatus', fileId, 'entityLinking', 'todo');
      return;
    }
    Meteor.call('updateGlobalStatus', fileId, 'done'); */
  }
}
