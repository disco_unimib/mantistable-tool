import validator from 'validator';
import nlp from 'wink-nlp-utils';
const isValidDate = require('is-valid-date');
const isDate = require('is-datestring');
const isImage = require('is-image');

export default function checkType(value,currency,iata) {
  const oldValue = value;
  let newValue = value.toLowerCase();

  if (validator.isEmpty(newValue) || nlp.string.retainAlphaNums(newValue) === '') {
    return 'empty';
  }

  let date = newValue.match(/[0-9]+-[0-9]+-[0-9]+/);
  if(date && (newValue.length - date[0].length) <= 19) {
      return 'date';
  }
  // Geographic coordinates (e.g. 46.1368155, 9.61057690000007)
  const geoCordsRegex = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/igm;
  // Geographic coordinates (e.g. 35°56′51″N 75°45′12″E)
  const alternativeGeoRegex = /^[0-9]+°[0-9]+(′|')[0-9]+(″|''|′′)\s*N\s*[0-9]+°[0-9]+(′|')[0-9]+(″|''|′′)\s*E/igm;

  if (oldValue.match(geoCordsRegex) !== null) {
    return 'GeoCoordinates';
  } else if (oldValue.match(alternativeGeoRegex) != null) {
    return 'GeoCoordinates';
  }
  if (validator.isNumeric(newValue.replace(/,/g, '')) || newValue.match(/[0-9],[0-9]+ ft | [0-9]+ m/) != null || newValue.match(/[0-9]+ m/) != null) 
    return 'numeric';
  // Address regex
  const addressRegex = /\d+\s+\w+\s+(?:st(?:\.|reet)?|ave(?:nue)?|lane|dr(?:\.|ive)?)/;
  if (newValue.match(addressRegex) !== null) return 'address';
  const address2regex = /^[\d]+[A-z\s,]+[\d]/;
  if (oldValue.match(address2regex)&&(!isNaN(parseInt(oldValue.charAt(oldValue.length), 10)))) return 'address';
  
  // It removes spaces and the '%' character from the string
  newValue = newValue.replace(/\s|%/g, '');

  if (validator.isHexColor(newValue)) return 'hexColor';
  
  const urlRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gm;
  if(newValue.match(urlRegex)!== null) return 'url';
  //if (validator.isURL(newValue)) return 'url';
  if (isImage(newValue)) return 'image';
  if (validator.isCreditCard(newValue)) return 'creditcard';
  if (validator.isEmail(newValue)) return 'email';
  //if (validator.isFQDN(newValue)) return 'fqdn';
  if (validator.isIP(newValue)) return 'ip';
  if (validator.isISBN(newValue)) return 'isbn';
  if (validator.isISO8601(newValue)) return 'iso8601';

  // Check if it contains "yes", "no", "1", "0", "true" or "false"
  const booleanRegex = /^(?:y(?:es)?|1)$|^(?:n(?:o)?|0)$/i;
  if ((booleanRegex).test(newValue)) return 'boolean';
  if (validator.isBoolean(newValue)) return 'boolean';

  //if (validator.isDecimal(newValue.replace(/,/g, ''))) return 'decimal';
  //if (validator.isInt(newValue.replace(/,/g, ''))) return 'integer';
  if (isDate(newValue)) return 'date';
  if (isValidDate(newValue)) return 'date';
  if(oldValue.length > 80) return 'description';
  // Check if it's an ID
  if (oldValue.length < 15) {
    // Remove everything which is not a word or a single whitespace
    let valuex = oldValue.replace(/[^\w\s]/g, ' ');
    valuex = nlp.string.removeExtraSpaces(valuex);
    valuex = valuex.trim();

    // They contain the number of letters, digits and words in the string
    const countletters = valuex.replace(/[^A-Z]/gi, '').length;
    const countdigits = valuex.replace(/[^0-9]/gi, '').length;
  
    const countws = valuex.replace(/[^\s]/gi, '').length;
    // If it is a single word composed of both digits and letters
    if (countws === 0) {
	if (countdigits > 0 && countletters > 0 && valuex.length < 5) {
        return 'ID';
      }

		//the small uppercase chars are not a currency code
		if(currency.includes(valuex)){
			return 'currency';
		}
      // If it has only uppercase characters
      if (valuex === valuex.toUpperCase()) {
		// If it is a small word with no digits
		if (valuex.length < 7 && countdigits === 0 && countletters > 0) {
			return 'ID';
		}
      }
    }

    // If it has 2 words and every character is uppercase
    if (countws === 1 && valuex === valuex.toUpperCase() && countdigits > 0 && oldValue.indexOf(" ")==-1) { //add countdigits > 0
      return 'ID';
    }

    /* If it has more than 2 words, every character is uppercase
        but with length limitations and no space
    */
    if (countws >= 1 && valuex === valuex.toUpperCase() && valuex.length < 15 && countdigits > 0 && oldValue.indexOf(" ")==-1) { //add countdigits > 0
      return 'ID';
    }
  }

  

  // Removes '-', "_", ":" characters
  newValue = newValue.replace(/-|_|:/g, '').trim();
  if (validator.isNumeric(`${nlp.string.retainAlphaNums(newValue)}`)) {
    return 'numeric';
  }

//check if it contains a IATA airport code
var words=oldValue.replace(/[-\/&,:;_]/g,' ');
if(oldValue.length==3||(words.length>3&&words.length<9)){
	words = words.split(' ');
	if(words.length<3){
		for(var i=0;i<words.length;i++){
			if(words[i].length>2&&words[i].length<5){
				var uc = words[i].toUpperCase();
				if(iata.includes(words[i].toUpperCase())){
					return 'iata';
				}
			}
		}
	}
}

let numsOfCommas = value.split(',').length; 
if(numsOfCommas > 1)
  return 'NoAnnotation';
  // Returns an empty strings if it can't find the right type
  return '';
}
