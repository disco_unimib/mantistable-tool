function ConvertObj(ln, nm) {
  const fields = ln.split('|');

  // Depending on the format type there are two different way to have the name
  let defaultName = null;
  if (fields.length < 4) {
    // It removes the double quotes that surround the string
    defaultName = fields[1].substring(1, fields[1].length - 1);
  } else {
    defaultName = nm;
  }

  const type = fields[0].substring(1, fields[0].length - 1);
  const name = fields[1].substring(1, fields[1].length - 1);

  const abbrs = fields[2].split('\,');
  for (let i = 0; i < abbrs.length; i += 1) {
    abbrs[i] = abbrs[i].substring(1, abbrs[i].length - 1);
  }

  let converterMul = 1.0;

  if (fields.length >= 4) {
    if (/\//.test(fields[3])) {
      // If there is a slash it is a fraction
      const numbTmp = fields[3].substring(1, fields[3].length - 1).split("\/");
      converterMul = numbTmp[0] / numbTmp[1];
    } else {
      converterMul = Number(fields[3].substring(1, fields[3].length - 1));
    }
  }

  let converterSum = 0;
  if (fields.length === 5) {
    if (/\//.test(fields[4])) {
      const numbTmp = fields[4].substring(1, fields[4].length - 1).split("\/");
      converterSum = numbTmp[0] / numbTmp[1];
    } else {
      converterSum = Number(fields[4].substring(1, fields[4].length - 1));
    }
  }

  return [defaultName, type, name, converterMul, converterSum, abbrs];
}


export default function converter() {
  // Maps from units of measurement to the relative quantity
  const area = Assets.getText('units/Area.txt');
  const currency = Assets.getText('units/Currency.txt');
  const density = Assets.getText('units/Density.txt');
  const electricurrent = Assets.getText('units/ElectricCurrent.txt');
  const energy = Assets.getText('units/Energy.txt');
  const flowrate = Assets.getText('units/Flowrate.txt');
  const force = Assets.getText('units/Force.txt');
  const frequency = Assets.getText('units/Frequency.txt');
  const fuelefficiency = Assets.getText('units/FuelEfficiency.txt');
  const informationunit = Assets.getText('units/InformationUnit.txt');
  const length = Assets.getText('units/Length.txt');
  const linearMassDensity = Assets.getText('units/LinearMassDensity.txt');
  const mass = Assets.getText('units/Mass.txt');
  const numbers = Assets.getText('units/Numbers.txt');
  const populationdensity = Assets.getText('units/PopulationDensity.txt');
  const power = Assets.getText('units/Power.txt');
  const pressure = Assets.getText('units/Pressure.txt');
  const speed = Assets.getText('units/Speed.txt');
  const temperature = Assets.getText('units/Temperature.txt');
  const time = Assets.getText('units/Time.txt');
  const torque = Assets.getText('units/Torque.txt');
  const voltage = Assets.getText('units/Voltage.txt');
  const volume = Assets.getText('units/Volume.txt');
  const convertion = [area, currency, density, electricurrent, energy, flowrate, force, frequency, fuelefficiency, informationunit, length, linearMassDensity,
    mass, numbers, populationdensity, power, pressure, speed, temperature, time, torque, voltage, volume];

  const convertions = [];
  convertion.forEach((element) => {
    const lines = element.split(/\r\n|\n/);
    const firstLineElem = lines[0].split('|');

    // It removes the double quotes that surround the string
    const elementName = firstLineElem[1].substring(1, firstLineElem[1].length - 1);

    lines.forEach((line) => {
      // It parses the string and return an object
      const tmp = ConvertObj(line, elementName);

      convertions.push(tmp);
    });
  });

  return convertions;
}


const Singleton = (function () {
  let instance;

  function createInstance() {
    const object = Converter();
    return object;
  }

  return {
    getInstance() {
      if (!instance) {
        instance = createInstance();
      }
      return instance;
    },
  };
}());


/*
   It tests lots of combinations of number representations to find
   which is used for the 'num' variable
*/
export function getType(num, str) {
  const collection = Singleton.getInstance();

  let modNumber = num;
  if (/,/.test(num)) {
    if (/\./.test(num)) {
      // Number with points and a comma for the decimal value (e.g. 123.456.789,0123)
      const structNumber = num.match(/[.,]/ig);
      if (structNumber.length > 1) {
        for (let i = 0; i < structNumber.length - 1; i += 1) {
          modNumber = modNumber.replace(structNumber[i], '');
        }
        modNumber = modNumber.replace(structNumber[structNumber.length - 1], '.');
      }
    } else if (num.match(/,/gi).length > 1) {
      // Number without points and with a comma for the decimal value (e.g. 33,2)
      modNumber = modNumber.replace(/,/ig, '');
    } else {
      // Number with commas but without a decimal value (e.g. 123,456,789)
      const modNumber2 = modNumber.split(',');
      if (/[0-9]{3}/.test(modNumber2[1])) {
        modNumber = modNumber.replace(/,/ig, '');
      } else {
        modNumber = modNumber.replace(/,/ig, '.');
      }
    }
  } else if (/\./.test(num)) {
    if (num.match(/\./gi).length > 1) {
      // Number with points but without a decimal value (e.g. 123.456.678)
      modNumber = modNumber.replace(/\./ig, '');
    }
  }

  for (const conv of collection) {
    // It matches the name
    if (str === conv[2]) {
      // [type, (modNumber * converterMul) + converterSum, defaultName]
      return [conv[1], (modNumber * conv[3]) + conv[4], conv[0]];
    }
    // It iterates through the abbreviations array
    for (const abbr of conv[5]) {
      if (str === abbr) {
        // [type, (modNumber * converterMul) + converterSum, defaultName]
        return [conv[1], (modNumber * conv[3]) + conv[4], conv[0]];
      }
    }
  }

  return null;
}
