import nlp from 'wink-nlp-utils';
import sparqlGetEntityIncludeToken from '../../utils/sparqlProxy';

export default function transformer(text, tableID) {
  /*  if(text.includes('?')) {
    text = getCorrectText(text);
  } */
  let cleanText;
  try {
    cleanText = getCleanText(text);
  }
  catch(e) {
    //console.log(e);
    cleanText = '';
    Log.insert({tableID: tableID, method: 'transformer', error: e.toString()});
  }
  let textForQuery = '';
  if(text.split('.').length > 1) {
    let c1 = cleanText.split(" ").join('" AND "');
    let c2 = cleanText.split(" ").filter(token => (token.length > 1)).join('" AND "');
    textForQuery = '"'+ c1 + '" OR "'+ c2 + '"';
  }
  else {
    let result = cleanText.split(": ");
    textForQuery = '"'+result.map(item => item.split(' ').join('" AND "')).join('" OR "')+'"';
  }
  return [cleanText, textForQuery];
}

function getCleanText(text) {
  let cleanText = '';
  let tokens =  nlp.string.tokenize(text, true);
  for(let i=0; i<tokens.length; i++) {
    if(tokens[i].value == ":") {
      cleanText = cleanText.substring(0, cleanText.length-1) + ": ";
    }
    if(tokens[i].tag == 'word' || tokens[i].tag == 'number') {
      if(tokens[i].value.includes("'")) {
        let index = tokens[i].value.indexOf("'");
        tokens[i].value = tokens[i].value.substring(0, index);
      }
      if(tokens[i].value != "")
        cleanText += tokens[i].value + ' ';
    }
    else if(tokens[i].value == '(') {
      let j = i + 1;
      while(j < tokens.length && tokens[j].value != ')') {
        j++;
      }
      i = j;
    }
  }
  return cleanText.trim().toLowerCase();
}

/* function getCorrectText(text) {
  let tokens = text.split(' ');
  tokens.forEach((token, i) => {
    if(token.includes('?')) {
      let words =  ['a', 'o', 'u'].map(item => token.replace('?', item).toLowerCase());
      let tmp = '"' + words.join('" OR "') + '"';
      let result = sparqlGetEntityIncludeToken(tmp);
      let buffer = '';
      result.forEach(item => buffer += item.l.value.toLowerCase() + ' ');
      buffer = buffer.trim().split(' ');
      let intersection = buffer.filter(item => words.includes(item.normalize('NFD').replace(/[\u0300-\u036f]/g, "")));
      if(intersection.length > 0) {
        let newToken = intersection[0].normalize('NFD').replace(/[\u0300-\u036f]/g, "");
        tokens[i] = newToken;
      }
    }
  });
  return tokens.join(' ');
} */
