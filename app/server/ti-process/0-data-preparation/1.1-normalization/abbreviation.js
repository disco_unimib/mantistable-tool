export function Abbreviate() {
  const abbreviations = [];

  // It fetches a map from abbreviations to the related meaning
  const myUnit = Assets.getText('units/Abbreviation.txt');

  /* For each line
    it splits the abbreviation from the related meaning
    and pushes both as a tuple in the abbreviations array
  */
  const lines = myUnit.split('\r').join('').split("\n");
  lines.forEach((line) => {
    abbreviations.push(line);
  });

  return abbreviations;
}


const Singletons = (function () {
  let instance;

  function createInstance() {
    const object = Abbreviate();
    return object;
  }

  return {
    getInstance() {
      if (!instance) {
        instance = createInstance();
      }
      return instance;
    },
  };
}());


// Taken a word, if it is an abbreviation it returns the meaning
export default function getAbbr(word) {
  const collection = Singletons.getInstance();
  for (const conv of collection) {
    var ln = conv.split("*");
	  if (word == ln[0]) {
      return ln[1];
    }
  }
  return null;
}
