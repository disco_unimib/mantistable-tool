// process require the table's ID. Return the json normalized and the number of the rows and
// the number of the columns

import winston from 'winston';
import abbreviation from './abbreviation';
import checkType from './check-type';
import transformer from '../../0-data-preparation/1.1-normalization/transformer';

/* It requires the table ID and it returns the json normalized
   and the number of rows and the number of columns
*/
export default function normalize(tableID, conv, currency, iata) {
  winston.log('silly', 'Normalize table');
  Alerts.insert({
    tableID, state: 'preprocessing', value: 'Normalization: start', type: 'text-warning',
  });
 
  const JSONdata = JSON.parse(Jsonfile.findOne({ _id: tableID }).data);
  const numRows = JSONdata.length;
  const numCols = Object.keys(JSONdata[0]).length;
  const keys = Object.keys(JSONdata[0]);
  const headerClean = [];
  
  for(let k of keys) {
    headerClean.push(transformer(k, tableID)[0]);
  }

  var units=[];
  for(var k=0;k<conv.length;k++){
    for(var j=0;j<conv[k][5].length;j++){
      units.push(conv[k][5][j].toLowerCase());
    }
  }

  var cellValue = new Map();
  var colType = getMyMap(numCols);
  var cols =  getMatrix(numCols);
  for (let i = 0; i < JSONdata.length; i += 1) {
    const jsonRow = JSONdata[i];
    let col = 0;

    for (const header in jsonRow) {
      // extract the cell value
      let matchValue = jsonRow[header].toString().trim();
      const old = matchValue;
      var abbr = abbreviation(matchValue.toLowerCase());
      if(abbr)
        matchValue=abbr; 

      let valueType;
      var tmp = cellValue.get(old);
      if(tmp) {
        valueType = tmp.type;
      }
      else {
        valueType = checkType(matchValue, currency, iata);
        cellValue.set(old, valueType);
      }  
      
      const countdigits = matchValue.replace(/[^0-9]/gi, '').length;
      if(valueType=="" && countdigits>0) {
        var val = matchValue.split(" ");
        for(var m = 0; m<val.length; m++) {
          if(units.includes(val[m].toLowerCase())){
            valueType="numeric";
            break;
          }
        }
      }
      
      let container=[valueType, matchValue, "Normalized"];
     
      let type = colType[col];
      if(type[valueType]) 
        type[valueType] = type[valueType] + 1;
      else
        type[valueType] = 1;
      
      if (!container[2].includes('Turned text into lowercase') && container[1] !== matchValue) {
        container[2].push('Turned text into lowercase');
      }

      let result = transformer(matchValue, tableID);
      cols[col].push({
        value_old: old,
        value: result[0],
        valueForQuery: result[1],
        type: valueType
      });
      
      col++;
    }
  }

  Alerts.insert({
    tableID,
    state: 'preprocessing',
    value: 'Normalization: done',
    type: 'text-warning',
  });

  let typeGS = Tables.findOne({_id: tableID});
  if(typeGS) 
    typeGS = typeGS.typeGS;
  
  TableDataCopy.update(
    {_id:tableID},
    {
      _id:tableID,
      typeGS: typeGS,
      header: headerClean,
      colType: colType,
      cols: cols
    },
    {upsert: true}
  );

  return [numRows, numCols, keys];
}


//create array di length cols, where each element is [] (empty array)
function getMatrix(cols) {
  var matrix = new Array(cols);
  for(var i=0; i<cols; i++) {
    matrix[i] = [];
  }
  return matrix;
}

//create array di length cols, where each element is {} (empty map)
function getMyMap(cols) {
  var map = new Array(cols);
  for(var i=0; i<cols; i++) {
    map[i] = {};
  }
  return map;
}