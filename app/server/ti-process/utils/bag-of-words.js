// UTILS functions
import nlp from 'wink-nlp-utils';
import pluralize from 'pluralize';
import transformer from '../0-data-preparation/1.1-normalization/transformer';

/**
 * It returns an array of string terms without terms with length == 1
 * @param text
 * @returns {*}
 */
export function bowText(text) {
  const tokens = nlp.string.tokenize(text);
  const tokensFiltered = tokens.filter((token) => {
    if (token.length > 1) return token;
  });

  return tokensFiltered;
}

export function getSingular(word) {
  return pluralize.singular(word);
}

export function getPlural(word) {
  return pluralize.plural(word);
}

export function removeStopWords(words) {
  let tokens = nlp.tokens.removeWords(words);
  let tokensFiltered = tokens.filter((token) => {
    if (token.length > 1) return token;
  });
  return tokensFiltered;
}


/**
 * Creates an object that represent the Bag of Words of the input array of tokens t.
 * If the input is a string, it creates a Bag of Words of every single character.
 * @param text
 * @returns {*}
 */
export function bowSetText(text) {
  const tokens = nlp.tokens.bow(text);
  return tokens;
}

/**
 * Search for synonyms.
 * @param text
 * @param lemmas
 * @returns {string} with terms concatenated through the '_' symbol
 */
export function getSynonym(text, lemmas) {
  let syn = '';
  const synz = '';
  if (typeof text !== 'string') {
    text = text.join('_');
  } else {
    if (text.indexOf(' ') < 0) {
      let toadd = "";
	  try{
		  toadd=OxfordSyn(text);
	  }catch(e){
		  Dictionary.insert({ kw: text, syn: text });
	  }
      if (toadd.length > 0) {
        syn += toadd;
      }
    }
    if (lemmas === 1) {
      let lemmasString = '';
      synz.forEach((success) => {
        lemmasString += `${success.synonyms.join(' ')} `;
      });
      syn = lemmasString;
    }
  }
  return syn;
}

// Search for meaningful synonyms in OxfordDictionaries
function OxfordSyn(word) {
  word = word.toLowerCase();
  const dt = Dictionary.find({ kw: word }, { $limit: 1 }).fetch();
  if (dt.length > 0) {
    return dt[0].syn.replace(',',' ');
  }
  const url = `https://od-api.oxforddictionaries.com:443/api/v1/entries/en/${word}/synonyms`;
  let result = '';
  try {
    result = HTTP.get(url, {
      headers: { Accept: 'application/json',
        app_id: '7b3b2f6c',
        app_key: 'f3968288bc4c4aca7a34277c6aa26359' },
		  });
  } catch (e) {
	
  }
  let ret = `${word},`;
  let sense2 = '';
  let sense1 = '';
  try {
      let tmp = '';
	  try{
		sense1 = result.data.results[0].lexicalEntries[0].entries[0].senses[0].subsenses;
		if (sense1) {
		  for (let i = 0; i < sense1.length; i++) {
			tmp += `${sense1[i].synonyms[0].text},`;
		  }
		}
	  }catch(e){
		  
	  }
      try{
		sense2 = result.data.results[0].lexicalEntries[0].entries[0].senses[1].subsenses;
		if (sense2) {
		  for (let i = 0; i < sense2.length; i++) {
			tmp += `${sense2[i].synonyms[0].text},`;
		  }
		}
	  }catch(e){
		  
	  }
	  try{
		tmp+=result.data.results[0].lexicalEntries[0].entries[0].senses[0].synonyms[0].text;
	  }catch(e){
		
	  }
    ret += tmp;
  } catch (e) {

  }
  Dictionary.insert({ kw: word, syn: ret });
  return ret.replace(',',' ');
}

export function getNameFromEntity(entity) {
  entity = decodeURIComponent(entity);
  let name = entity.substring(28).split("_").join(" ").toLowerCase();
  return transformer(name)[0];
}

export function getRowContext(cols, row, col) {
  let tmp = '';
  for(let i=0; i<cols.length; i++) {
    if(i != col)
      tmp += cols[i][row].value + ' ';
  }
  tmp = tmp.trim();
  tmp = bowSetText(bowText(tmp));
  return  tmp;
}

export function getHeaderContextFromHeaderTable(header) {
  return bowSetText(header);
}

export function getBowSet(cell, entity, abstract) {
  let bowSetCell = bowSetText(bowText(cell));
  let bowSetEntity = bowSetText(bowText(getNameFromEntity(entity)));
  let bowSetAbstract =  bowSetText((bowText(abstract.toLowerCase())));
  return [bowSetCell, bowSetEntity, bowSetAbstract];  
}