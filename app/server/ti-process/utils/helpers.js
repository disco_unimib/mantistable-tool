const fs = require('fs');

const filesT2D = JSON.parse(fs.readFileSync('assets/app/tables/T2Dv2/tableIDChallange.json'));
const filesLimaye = JSON.parse(fs.readFileSync('assets/app/tables/Limaye200/limayeTables.json'));
const filesRound2 = JSON.parse(fs.readFileSync('assets/app/tables/Round2/errorTables.json'));

export function loadTables(typeGS) {
  console.log('*************************** Loading tables in Mongo');
  let index = 1;
  let files = getFiles(typeGS);
  
  for(let file of files) {
    console.log(`----------------TAB ${index} di ${files.length}`);
    let data = Assets.getText(`tables/${typeGS}/converted/${file}.json`);
    Tables.insert({
      _id: file,
      name: file,
      typeGS: typeGS,
      file_name: `${file}.json`,
      date: new Date(),
    });

    Jsonfile.insert({
      _id: file,
      data,
    });

    InfoTable.insert({
      _id: file,
      typeGS: typeGS,
    });

    Meteor.call('setInitialInfoTable', file);
   /*  if(index == 1000)
      break;  */
    index++;
  }
}

export function delTables(typeGS) {
  console.log('*************************** Deleting tables from Mongo');
  let index = 1;
  let files = getFiles(typeGS);

  for (let file of files) {
    console.log(`----------------TAB ${index} di ${files.length}`);
    Tables.remove({ _id: file });
    Jsonfile.remove({ _id: file });
    InfoTable.remove({ _id: file });
    index++;
  }
}

function getFiles(typeGS) {
  if(typeGS == 'T2Dv2') 
    files = filesT2D;
  else if(typeGS == 'Limaye200')
    files = filesLimaye;
  else
    files = filesRound2;
  return files;    
}
