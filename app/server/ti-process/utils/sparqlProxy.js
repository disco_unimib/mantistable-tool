import sparql from '../utils/client';

const sparqlClient = new sparql('http://dbpedia.org/sparql/'); //http://34.65.101.67:8890/sparql/


export function sparqlGetEntities(textForQuery) {
  let queryEntities = `SELECT DISTINCT str(?s) as ?s str(?abstract) as ?abstract 
    WHERE {
      {
        ?s dbo:abstract ?abstract.
        ?s a ?type.
        ?s rdfs:label ?label.
        ?label <bif:contains> '(${textForQuery})'.
      }
      FILTER NOT EXISTS { ?s dbo:wikiPageRedirects ?r2}.
      FILTER (!strstarts(str(?s), 'http://dbpedia.org/resource/Category:')). 
      FILTER (!strstarts(str(?s), 'http://dbpedia.org/property/')).
      FILTER (!strstarts(str(?s), 'http://dbpedia.org/ontology/')). 
      FILTER (strstarts(str(?type), 'http://dbpedia.org/ontology/')). 
      FILTER (lang(?abstract) = 'en').
    }
    ORDER BY ASC(strlen(?label))
    LIMIT 50`;
  //console.log(queryEntities);
  try {
    resultEntities = sparqlClient.query({
      graph: 'http://dbpedia.org',
      query: queryEntities,
    });
	  resultEntities = JSON.parse(resultEntities).results.bindings;
  } 
  catch (err) {
    console.log(err);
    console.log(queryEntities);
    resultEntities = [];
  }
  return resultEntities;
}

/**
 * It returns the list of classes relatives about the winning entity
 * @param winningEntities
 * @returns {*}
 */
export function sparqlGetType(winningEntities) {
  let query = `SELECT DISTINCT str(?type) as ?type
    WHERE {
      {
        <${winningEntities}> a ?type.
        FILTER(!strstarts(str(?type), 'http://dbpedia.org/class/yago/Wikicat')).
      }
    }`;
  let result = [];
  try {
    result = sparqlClient.query({
      graph: 'http://dbpedia.org',
      query: query,
    });
	  result = JSON.parse(result).results.bindings;
  } 
  catch (err) {
    console.log(err);
    result = [];
  }
  return result;
}

/** *********************************
 * RELATIONSHIP ANNOTATION QUERY
 ********************************** */

export function sparqlGetNErel(winningEntities, subjectEntity, subjectConcept, colConcept) {
  let relObjQuery = `SELECT DISTINCT ?p (count(?p) as ?count)
      WHERE {
        {
          ?s ?p ?o.
          VALUES ?o {${winningEntities}}. 
          ?s a <${subjectConcept}> .
          FILTER (strstarts(str(?p), str("http://dbpedia.org/ontology/"))).
        
        } 
        UNION {
          ?s ?p ?o.
          VALUES ?s {${subjectEntity}}.
          ?o a <${colConcept}>.
          FILTER (strstarts(str(?p), str("http://dbpedia.org/ontology/"))).
        }
      }`;
  console.log(relObjQuery);    
  let relObj;
  try {
    relObj = sparqlClient.query({
      graph: 'http://dbpedia.org',
      query: relObjQuery,
    });
	relObj = JSON.parse(relObj).results.bindings;
  } 
  catch (err) {
    console.log(relObjQuery);
    relObj = [];
  }
  return relObj;
}

export function sparqlGetLITrel(winningEntities, subjectConcept, colDataType) {
  let relObjQuery = `SELECT DISTINCT ?p (count(?p) as ?count)
    WHERE {
      {
        ?s ?p ?o.
        VALUES ?s {${winningEntities}}. 
        ?s a <${subjectConcept}> .
        FILTER (strstarts(str(?p), str("http://dbpedia.org/ontology/"))).
        FILTER (${colDataType}(?o)).
        FILTER (!strstarts(str(?p), "http://dbpedia.org/ontology/wikiPageRevisionID")).
        FILTER (!strstarts(str(?p), "http://dbpedia.org/ontology/wikiPageID")).
        FILTER (!strstarts(str(?p), "http://dbpedia.org/ontology/abstract")).
      } 
    }`
  //console.log(relObjQuery);  
  let relObj;
  try {
    relObj = sparqlClient.query({
      graph: 'http://dbpedia.org',
      query: relObjQuery,
      timeout: 5000,
    });
    relObj = JSON.parse(relObj).results.bindings;
  } 
  catch (err) {
    console.log(relObjQuery);
    relObj = [];
  }
  return relObj;
}

/** *********************************
 * ENTITY LINKING QUERY
 ********************************** */
export function sparqlGetFinalEntity(textForQuery, type, OR) {
  if(OR)
    textForQuery = textForQuery.split('AND').join('OR');
  let query = `SELECT DISTINCT str(?s) as ?s str(?abstract) as ?abstract
    WHERE {
      {
        ?s dbo:abstract ?abstract.
        ?s rdfs:label ?l.
        ?l <bif:contains> '(${textForQuery})'.
        ?s rdf:type ?t.
        FILTER regex(str(?t), "${type}", "i").
        FILTER(!regex(str(?t), "BasedOn", "i")).
      }`;
  if(!OR) {
    query += `UNION {
      ?s dbo:abstract ?abstract.
      ?abstract <bif:contains> '(${textForQuery})'.
      ?s rdf:type ?t.
      FILTER regex(str(?t), "${type}", "i").
      FILTER(!regex(str(?t), "BasedOn", "i")).
    }    
    UNION {
        ?s dbo:abstract ?abstract.
        ?s2 dbo:wikiPageRedirects ?s.
        ?s2 rdfs:label ?l.
        ?l <bif:contains> '(${textForQuery})'.
        ?s rdf:type ?t.
        FILTER regex(str(?t), "${type}", "i").
        FILTER(!regex(str(?t), "BasedOn", "i")). 
    }`;
  }
  query += "FILTER (lang(?abstract) = 'en').} LIMIT 50"; 
  let resultEntities;
  //console.log(query);
	try {
    resultEntities = sparqlClient.query({
      graph: 'http://dbpedia.org',
      query: query,
    });
    resultEntities = JSON.parse(resultEntities).results.bindings;
  } 
  catch (err) {
    console.log(err);
	  console.log(query);
    resultEntities = [];  
  }
  return resultEntities;
}

export default function sparqlGetEntityIncludeTokens(tokens) {
  let query = `SELECT DISTINCT str(?l) as ?l
    WHERE {
        ?s rdfs:label ?l.
        ?l <bif:contains> '(${tokens})'.
        ?s rdf:type ?t.
      
    } LIMIT 10`;
  try {
    resultEntities = sparqlClient.query({
      graph: 'http://dbpedia.org',
      query: query,
    });
    resultEntities = JSON.parse(resultEntities).results.bindings;
  } 
  catch (err) {
    console.log(err);
    console.log(query);
    resultEntities = [];
  }
  return resultEntities;
}