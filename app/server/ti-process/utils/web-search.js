import { HTTP } from 'meteor/http';

export function googleWebSearch(query) {
  /* HTTP method return a field "data" that is a JSON-able object to stringify and use as
  // the HTTP request body.
  NOTE: it is a synchrounous method because asynchCallBack param is missing */
  let ws = HTTP.call('GET', 'https://www.googleapis.com/customsearch/v1', {
    params: {
      key: 'AIzaSyB4KrTL5hMc7IdFCLT_1-sJLj3no0OlrOA',
      cx: '002654517660687702503:symoby5u9t0',
      num: '10',
      q: query,
    },
  });
  // Google Apis return a field "items" that is the current set of custom search results.
  ws = ws.data.items;

  let results = [];
  if (ws && typeof ws[0] !== 'undefined') {
    ws.map((item) => {
      results.push({
        title: item.title,
        snippet: item.snippet,
      });
    });
  }

  return results;
}
