var http = require('http');
var request = require('sync-request');
var querystring = require('querystring');

var sparql = function (endpoint, graph) {
  if (!endpoint) throw new Error('No endpoint provided!');

  this.endpoint = endpoint;
  this.graph = graph || '/';
};

sparql.prototype.query = function (options, cb) {
  if (typeof(options) === 'string') {
    options = {
      graph: null,
      query: options
    };
  }

  var graph = options.graph || this.graph;
  var query = "PREFIX dbo: <http://dbpedia.org/ontology/> " + options.query;

  if (!query) return cb(new Error('No query provided!'));
  if (!graph) return cb(new Error('No graph provided!'));




  var httpParams = querystring.stringify({
    'default-graph-uri': graph,
    query: query,
    'should-sponge': 'grab-everything',
    format: 'application/sparql-results+json',
    timeout: 30000
  });

  var requestDefaults = {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/sparql-results+json'
    },
    body: httpParams
  };

  var uri = this.endpoint + '?' + httpParams;


  prova = request('POST', this.endpoint, requestDefaults);

  return prova.body.toString();
};

module.exports = sparql;

