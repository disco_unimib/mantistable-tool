import {
  getSingular,
  getPlural,
  getNameFromEntity,
  bowText
} from './bag-of-words';


function editDistance(str1, str2) {
  const distances = [];

  for (var i = 0; i <= str1.length; ++i) {
    distances[i] = [i];
  }
  for (var i = 0; i <= str2.length; ++i) {
    distances[0][i] = i;
  }

  for (let j = 1; j <= str2.length; ++j) {
    for (var i = 1; i <= str1.length; ++i) {
      distances[i][j] = str1[i - 1] === str2[j - 1] // if the characters are equal
        ? distances[i - 1][j - 1] // no operation needed
        // else
        : Math.min.apply(Math, [ // take the minimum between
          distances[i - 1][j] + 1, // a  deletion
          distances[i][j - 1] + 1, // an insertion
          distances[i - 1][j - 1] + 1, // a  substitution
        ]);
    }
  }
  return distances[str1.length][str2.length];
}

export function getEditDistanceScore(valueInCell, nameOfEntity) {
  return parseFloat((editDistance(valueInCell, nameOfEntity) / Math.max(valueInCell.length, nameOfEntity.length)).toFixed(2));
}

export function getRowContextScore(bowSetAbstract, bowSetHeader, bowSetRow) {
  let score = 0;
  let k1 = Object.keys(bowSetAbstract);
  let k2 = Object.keys(bowSetHeader);
  let k3 = Object.keys(bowSetRow);
  let s1 = k1.filter(word => (k2.includes(getSingular(word)) || k2.includes(getPlural(word))));
  let s2 = k1.filter(word => (k3.includes(getSingular(word) || k3.includes(getPlural(word)))));
  let ec = s1.concat(s2);
  score += ec.length;
  return [score, ec];
}

export function getCellContextScore(bowSetCell, bowSetEntity, bowSetAbstract) {
  [k1, k2, k3] = getKeys(bowSetCell, bowSetEntity, bowSetAbstract);
  let s1 = k1.filter(word => k2.includes(word));
  let s2 = k1.filter(word => k3.includes(word));
  let score = s1.length + s2.length;
  return parseFloat(score.toFixed(2));
}

function getKeys(bowSet1, bowSet2, bowSet3) {
  let k1 = Object.keys(bowSet1);
  let k2 = Object.keys(bowSet2);
  let k3 = Object.keys(bowSet3);
  return [k1, k2, k3];
}

/* 
Get Scores compute to obtain winning entity 
*/
export function getScores(cell, entity, bowSetCell, bowSetEntity, bowSetAbstract, bowSetHeaderContext, bowSetRowContext, resultEntities) {
  let edScore = getEditDistanceScore(cell, entity);
  let tmp = getRowContextScore(bowSetAbstract, bowSetHeaderContext, bowSetRowContext)
  let rowContextScore = tmp[0];
  let cellContextScore = getCellContextScore(bowSetCell, bowSetEntity, bowSetAbstract);
  resultEntities.push({
    editDistanceScore: edScore,
    rowContextScore: rowContextScore,
    cellContextScore: cellContextScore,
    ec: tmp[1]
  });
  return {
    editDistanceScore: edScore, 
    rowContextScore: rowContextScore, 
    cellContextScore: cellContextScore
  };
}

