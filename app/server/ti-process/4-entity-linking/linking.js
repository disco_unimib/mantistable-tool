import { sparqlGetFinalEntity} from '../utils/sparqlProxy';
import { getFinalScore } from '../utils/scores';
import {
  getRowContext,
  getHeaderContextFromHeaderTable,
} from '../utils/bag-of-words';
import { getWinningEntity } from '../2-concept-datatype-annotation/entities';
const fs = require('fs');
//const ceaGT = getAnnotations(fs.readFileSync('assets/app/CEA.csv').toString().split('\r\n'));

export default function getEntityLinking(tableID) {
  const infoTable = InfoTable.find({ _id: tableID }).fetch();
  const numRows = infoTable[0].numRows;
  const neCols = infoTable[0].neCols;
  const indexSubCol = infoTable[0].subCol;
  const tdc = TableDataCopy.findOne({
    _id: tableID,
  });
  const header = tdc.header;
  console.log(`Entity Linking started for ${tableID}`);
  neCols.forEach(neCol => linking(neCol.type.substring(28), tdc.cols, neCol.index, header));
  console.log(`Entity Linking finished ${tableID}`);

  TableDataCopy.update({
    _id: tableID
  },
  {
    $set: {
      cols: tdc.cols
    }
  });
}

export function linking(type, cols, col, header) {
  let bwHeaderContext = getHeaderContextFromHeaderTable(header);
  let row = 0;
  let values = new Map();
  let resultEntitiesFinal = [];
  let column = cols[col];
  for (let c of column) {
    let cell = c.value;
    let tmpWE = values.get(cell);
    let winningEntity = '';
    if (tmpWE != undefined) {
      winningEntity = tmpWE;
    }
    else {
      let entities = sparqlGetFinalEntity(c.valueForQuery, type);
      if(entities.length == 0)
        entities = sparqlGetFinalEntity(c.valueForQuery, type, true); 
      let bwRowContext = getRowContext(cols, row, col); 
      resultEntitiesFinal = [];
      winningEntity = getWinningEntity(cell, entities, bwHeaderContext, bwRowContext, resultEntitiesFinal);
      console.log("result entity: "+entities.length);
    }
    console.log("row: "+row);
    values.set(cell, winningEntity);
    column[row].linked_entity = winningEntity;
    cols[col][row].finalEntities = resultEntitiesFinal;
    row++;
  }
}