import {computeAW} from '../2.2-subject-column-detection/sub-detection'
import transformer from '../../0-data-preparation/1.1-normalization/transformer';

function getDataType(valueType) {
  let dataType;
  switch (valueType) {
    case 'GeoCoordinates':
      dataType = {
        datatype: 'xsd:float',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#float',
      };
      break;
    case 'address':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'hexColor':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'url':
      dataType = {
        datatype: 'xsd:anyURI',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#anyURI',
      };
      break;
    case 'numeric':
      dataType = {
        datatype: 'xsd:float;xsd:integer;xsd:double',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#float;http://www.w3.org/2001/XMLSchema#integer;http://www.w3.org/2001/XMLSchema#double',
      };
      break;
    case 'image':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'creditcard':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'email':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'ip':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'isbn':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'iso8601':
      dataType = {
        datatype: 'xsd:date',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#date',
      };
      break;
    case 'boolean':
      dataType = {
        datatype: 'xsd:boolean',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#boolean',
      };
      break;
    case 'date':
      dataType = {
        datatype: 'xsd:date',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#date',
      };
      break;
    case 'ID':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'currency':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'description':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    case 'iata':
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
      break;
    default:
      dataType = {
        datatype: 'xsd:string',
        datatypeUrl: 'http://www.w3.org/2001/XMLSchema#string',
      };
  }

  return dataType;
}

export default function setInfoTable(fileId) {
  // It uses aggregation with Mongodb to count occurrences of types
  Alerts.insert({
    tableID: fileId,
    state: 'columnsAnalysis',
    value: 'Info-table: start',
    type: 'text-warning',
  });

  const JSONdata = JSON.parse(Jsonfile.findOne({ _id: fileId }).data);
  var numRows = JSONdata.length;
  const numCols = Object.keys(JSONdata[0]).length;
  const keys = Object.keys(JSONdata[0]);
  for (let i = 0; i <keys.length;i++){
	  keys[i] = transformer(keys[i])[0];
  }
  // Costant values which are used often in the code
  var neCols = [];
  var litCols = [];
  var noAnnCols = [];

  Alerts.insert({
    tableID: fileId,
    state: 'columnsAnalysis',
    value: 'Determinate NE and Literal columns',
  });
  let colType = TableDataCopy.findOne({
    _id: fileId,
  }).colType;
 
  for (let i = 0; i < numCols; i++) {
    let dataType = colType[i];
    let valueType;
    var numType = 0;
    var numEmpty = 0;
    var maxOfType;
    var max = 0;
    let typeFreq = [];
    for (var k in dataType) {
  
      if (k == '') 
        numNoType = dataType[k];
      else if (k == 'empty') 
        numEmpty += dataType[k];
      else 
        numType += dataType[k];
      
      if(dataType[k] > max) {
        max = dataType[k];
        maxOfType = k;
      }
      
      let rate = (dataType[k] / numRows).toFixed(2);
      typeFreq.push({
        name: k,
        numOcc: dataType[k],
        rate: rate
      });

    }

    let description = false;
    if(computeAW(fileId, i) > 6) {
      description = true;
      maxOfType='description';
    }

    if ((numType > ((numRows-numEmpty) * 0.60) && (numEmpty <= (numRows * 0.70))) || description) {
      valueType = maxOfType;
    }
    // Header check, Mongo doesn't like '.', '$', '\' and empty string in key fields
    if (keys[i] === '') {
      keys[i] = '-';
    }

    if (numEmpty >= numRows * 0.70 || valueType == 'NoAnnotation' || valueType == 'ID') {

      noAnnCols.push({
        index: i,
        header: keys[i],
        type: null
      });

    } 
    else if (valueType) {
      
        litCols.push({
          index: i,
          header: keys[i],
          type: valueType,
          dataType: getDataType(valueType),
          colType: typeFreq
        });
    } 
    else {

      neCols.push({
        index: i,
        header: keys[i],
        type: valueType,
      });

    }
  }
  
  Alerts.insert({
    tableID: fileId,
    state: 'columnsAnalysis',
    value: `NE are: ${JSON.stringify(neCols)}`,
  });

  Alerts.insert({
    tableID: fileId,
    state: 'columnsAnalysis',
    value: `Literal are: ${JSON.stringify(litCols)}`,
  });

  InfoTable.update({ _id: fileId },
    {
      $set: {
    _id: fileId,
        numCols,
        numRows,
        neCols,
        litCols,
        noAnnCols,
        createdAt: new Date(),
      },
    }, { upsert : true });
}