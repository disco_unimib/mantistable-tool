import stopWord from 'stopword';
import nlp from 'wink-nlp-utils';
import { googleWebSearch } from '../../utils/web-search';
import { bowText, bowSetText } from '../../utils/bag-of-words';
import { Promise } from "meteor/promise";
/*
 * For every column => number of 'empty' / number of rows
 * @param fileid
 * @returns {number}
 */
function computeEMC(fileid, numRows) {
  let dataType = History.findOne({tableID: fileid, type:'colType'}).value;
  var emptyCells = 0;
  for( var k in dataType) {
    if (k == 'empty') {
      emptyCells = dataType[k];
    }
  }
  return parseFloat(emptyCells / numRows) * 10;
}

function computeCM(fileid, table, colIndex) {
	var ct = Context.findOne({tableID: fileid, col: colIndex}).cellcontexts[1];
	var count =0;
	for(var i=0;i<ct[2].length;i++){
		
		var h = ct[2][i];
		if(ct[0][h])
			count++;
		if(ct[1][h])
			count++;
		if(ct[3][h])
			count++;
	}
	console.log("DONE CM");
	console.log(count);
	return count;
}
/**
 * It counts, taken an element, how many times it occurs
 * @param fileid
 * @returns {number}
 */
function computeUC(fileid, table, col) {
  col = col.split(".").join("");
  const uniqueCells = Promise.await(Datas.aggregate(
    { $match: { tableID: fileid } },
    {
      $group: {
        _id: '$stringDoc.'+col,
        count: { $sum: 1 },
      }
    }
  ).toArray());
  // Number of groups for every column / number of rows
  return parseFloat(Object.keys(uniqueCells).length / table.numRows);
}

/**
 * It returns an average of words of every column
 * @param fileid
 * @returns {number}
 */
function computeAW(fileid, table, col) {
  const textResults = Datas.find({ tableID: fileid }, {
    fields: {
      [`stringDoc.${  col}`]: 1,
      _id: 0,
    },
  }).map(doc => doc.stringDoc[col]);

  let numberOfWords = nlp.string.removePunctuations(JSON.stringify(textResults));
  numberOfWords = nlp.string.removeExtraSpaces(numberOfWords);
  numberOfWords = stopWord.removeStopwords(numberOfWords.split(' '));
  numberOfWords = numberOfWords.join(' ');
  numberOfWords = numberOfWords.split(' ').length;

  return numberOfWords / table.numRows;
}

function computeWS(fileid, col, colIndex, litCols) {
  var bw = WebSearch.findOne({tableID: fileid, headerIndex: colIndex});
  if(bw)
    bw = bw.webSearch
  else {
    var litHeaders = '';
    for(var litCol of litCols) {
      litHeaders += litCol.header.toLowerCase() + ' ';  
    }
    var query = col.toLowerCase() +' '+litHeaders;
    var webSearchText = googleWebSearch(query);
  
    var text = '';
    for(var tmp of webSearchText)
      text += tmp.title.toLowerCase() + ' ' + tmp.snippet.toLowerCase();
    text = text.split('.').join('');
    bw = bowSetText(bowText(text));
    WebSearch.insert({
      tableID: fileid,
      header: col,
      headerIndex: colIndex,
      webSearch: bw
    });
  }
  var token = col.split(' ');
  var tmp = '';
  var wsScore = 0;
  for(var t of token) {
    tmp = t.toLowerCase();
    wsScore += bw[tmp] ? bw[tmp] : 0;  
  }
  return wsScore;
}

export default function getSubCol(fileid) {
  const table = InfoTable.findOne({ _id: fileid });
  // Initializing scores
  var emc = [];
  var uc = [];
  var df = [];
  var ac = [];
  var aw = [];
  var ws = [];
  var cm = [];
  var finalScore = [];
 
  Alerts.insert({ tableID: fileid,
    state: 'columnsAnalysis',
    value: 'Determinate subject column' });

  var score = [];
  var firstNE = 0;
  var neCols = table.neCols;
  for (var i = 0; i < neCols.length; i++) {
    var element = {};
    var col = neCols[i].header;
    var colIndex = neCols[i].index;
    if (i == 0) 
      firstNE = colIndex;
    var myProps = 'stringDoc.myProps.type' + [colIndex] + '';
    // EMC
    emc.push(computeEMC(fileid, table.numRows).toFixed(3));
    element.emc = parseFloat(emc[i]);
	cm.push(computeCM(fileid, table, colIndex));
	element.cm = parseFloat(cm[i]);
    // UC
    uc.push(computeUC(fileid, table, col).toFixed(1));
    element.uc = parseFloat(uc[i]);
    // DF: distance from the column analyzed and the first NE column
    df.push(colIndex - firstNE);
    element.df = df[i];
    // AW
    aw.push(computeAW(fileid, table, col).toFixed(1));
    element.aw = parseFloat(aw[i]).toFixed(1);
    // WS
    var wsScore = computeWS(fileid, col, colIndex, table.litCols);
    if(wsScore < 5)
      wsScore += 5;
    ws.push(wsScore);
    element.ws = ws[i];
    score.push(element);
  }

  for (var i = 0; i < table.neCols.length; i++) {
    var emcNorm = 0;
    if (emc[i] != 0) {
      emcNorm = emc[i] / Math.max(...emc);
    }
    var wsNorm = ws[i] / Math.max(...ws);
    var ucNorm = uc[i] / Math.max(...uc);
	var cmNorm=0;
	if(Math.max(...cm)>0)
		cmNorm = cm[i] / Math.max(...cm);
    var dfFinal = df[i] + 1;
    var awFinal = aw[i] / Math.max(...aw);
    if(table.neCols.length < 3)
      weightWS = 2;
    else
      weightWS = 0;
    finalScore.push((ucNorm + 2*cmNorm + 2*wsNorm - emcNorm) / Math.sqrt(dfFinal));
    element = score[i];
    element.emcNorm = emcNorm.toFixed(1);
    element.ucNorm = ucNorm.toFixed(1);
    element.dfFinal = dfFinal;
    element.awFinal = awFinal.toFixed(1);
	element.cmFinal = cmNorm;
    element.wsNorm = wsNorm.toFixed(1);
    element.finalScore = finalScore[i].toFixed(2);
  }
 
  var indexSubCol = finalScore.indexOf(Math.max(...finalScore));
  var neColsIndex = -1; //value for index subCol for table without subject column
  if(indexSubCol != -1)
    neColsIndex = neCols[indexSubCol].index;

  Alerts.insert({
    tableID: fileid,
    state: 'columnsAnalysis',
    value: `The subject column is the column with index: ${neColsIndex}`
  });

  for(var i=0; i<neCols.length; i++) 
    neCols[i].score = score[i];
  
  InfoTable.update({ _id: fileid }, { $set: { 'subCol': neColsIndex, 'neCols': neCols } });
}