import { Promise } from "meteor/promise";
/*
 * For every column => number of 'empty' / number of rows
 * @param fileid
 * @returns {number}
 */
function computeEMC(tableID, col, numRows) {
  let dataType = TableDataCopy.findOne({_id: tableID}).colType[col];
  var emptyCells = 0;
  for( var k in dataType) {
    if (k == 'empty') {
      emptyCells = dataType[k];
    }
  }
  return parseFloat(emptyCells / numRows) * 10;
}

/**
 * It counts, taken an element, how many times it occurs
 * @param fileid
 * @returns {number}
 */
export function computeUC(fileid, numRows, col) {
  const uniqueCells = Promise.await(TableDataCopy.aggregate([
      {$match: {_id: fileid}},  
      {
        $project:
         { 
            _id: 0, 
            first: { $arrayElemAt: [ "$cols", col ] }
         }
      },
      {$unwind: "$first" },
      {$group: 
          {
            _id: '$first.value', 
            count: { $sum: 1 }
          }
      }
   ]).toArray());
  // Number of groups for every column / number of rows
  return Object.keys(uniqueCells).length / numRows;
}

/**
 * It returns an average of words of every column
 * @param tableID
 * @param indexCol
 * @returns {number}
 */
export function computeAW(tableID, indexCol) {
  const cols = TableDataCopy.findOne({ _id: tableID }).cols[indexCol];
  const numRows = cols.length;
  let count = 0;

  for(let r of cols) 
    count += r.value.split(' ').length; 
  
  return count / numRows;
}

export default function getSubCol(fileid) {
  const table = InfoTable.findOne({ _id: fileid });
  // Initializing scores
  var emc = [];
  var uc = [];
  var df = [];
  var aw = [];
  var finalScore = [];
 
  Alerts.insert({ tableID: fileid,
    state: 'columnsAnalysis',
    value: 'Determinate subject column' });

  var score = [];
  var firstNE = 0;
  var neCols = table.neCols;
  for (var i = 0; i < neCols.length; i++) {
    var element = {};
    var colIndex = neCols[i].index;
    if (i == 0) 
      firstNE = colIndex;
    // EMC
    emc.push(computeEMC(fileid, colIndex, table.numRows).toFixed(3));
    element.emc = parseFloat(emc[i]);
    // UC
    uc.push(computeUC(fileid, table.numRows, colIndex).toFixed(1));
    element.uc = parseFloat(uc[i]);
    // DF: distance from the column analyzed and the first NE column
    df.push(colIndex - firstNE);
    element.df = df[i];
    // AW
    aw.push(computeAW(fileid, colIndex).toFixed(1));
    element.aw = parseFloat(aw[i]).toFixed(1);
    score.push(element);
  }

  for (var i = 0; i < table.neCols.length; i++) {
    var emcNorm = 0;
    if (emc[i] != 0) {
      emcNorm = emc[i] / Math.max(...emc);
    }
    var ucNorm = uc[i] / Math.max(...uc)
    var dfFinal = df[i] + 1;
    var awFinal = aw[i] / Math.max(...aw);
   
    finalScore.push((2*ucNorm + awFinal - emcNorm) / Math.sqrt(dfFinal));
    element = score[i];
    element.emcNorm = emcNorm.toFixed(1);
    element.ucNorm = ucNorm.toFixed(1);
    element.dfFinal = dfFinal;
    element.awFinal = awFinal.toFixed(1);
    element.finalScore = finalScore[i].toFixed(2);
  }

  for(var i=0; i<neCols.length; i++) 
    neCols[i].score = score[i];

  let finalScoreMax = Math.max(...finalScore).toFixed(2);
  let tmp = neCols.filter(item => item.score.finalScore == finalScoreMax)[0]
  indexSubCol = tmp ? tmp.index : undefined;

  Alerts.insert({
    tableID: fileid,
    state: 'columnsAnalysis',
    value: `The subject column is the column with index: ${indexSubCol}`
  });

  InfoTable.update({ _id: fileid }, { $set: { 'subCol': indexSubCol, 'neCols': neCols } });
}