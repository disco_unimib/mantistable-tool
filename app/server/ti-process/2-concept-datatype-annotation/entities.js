import {
  sparqlGetEntities,
  sparqlGetType
} from '../utils/sparqlProxy';
import {
  getRowContext,
  getHeaderContextFromHeaderTable,
  getBowSet,
  getNameFromEntity
} from '../utils/bag-of-words';
import {
  getScores
} from '../utils/scores';

const numberOfRows = 30;
const fs = require('fs'); 
const ontologyClass = new Map(JSON.parse(fs.readFileSync('assets/app/ontologyClass.json')));
const ontologyGraph = JSON.parse(fs.readFileSync('assets/app/OntologyGraph.json'));
const ontologyGraphCT = JSON.parse(fs.readFileSync('assets/app/OntologyGraphCT.json'));
const CTATarget = new Map(JSON.parse(fs.readFileSync('assets/app/tables/Round2/CTATarget.json'))); 
let debug;

export default function getEntities(tableID, debugIsOn, challenge) {
  Annotations.remove({ tableID: tableID });
  Alerts.remove({ tableID: tableID });
  debug = debugIsOn;
  // Get infos about the table
  const infoTable = InfoTable.findOne({ _id: tableID });
  const tdc = TableDataCopy.findOne({_id:tableID});
  const cols = tdc.cols;
  const header = tdc.header;
  let neCols = infoTable.neCols;
  const newNoAnnColsIndex = [];
  const subCol = infoTable.subCol;
  const totalRowsTable = infoTable.stats.row;
  let headerContext = getHeaderContextFromHeaderTable(header);
  let indexes = getIndexesRows(totalRowsTable);

  if(challenge) {
    neCols = [];
    let tmp = CTATarget.get(tableID);
    let colsTarget = tmp ? tmp : [];
    for(let col of colsTarget)
      neCols.push({index: col});
  }
  let weCols = {};
  neCols.forEach((neCol) => {
    let winningEntities = [];
    getEntitiesAndDisambiguation(cols, indexes, neCol.index, headerContext, winningEntities, tableID);
    let noAnnotation = checkIsNoAnnotation(winningEntities);
    noAnnotation = false;
    if(!challenge && noAnnotation && neCol.index != subCol) {
      newNoAnnColsIndex.push(neCol.index);
    }
    else {
      let tmp = getType(winningEntities, totalRowsTable, header[neCol.index]);
      let type = tmp[0];
      let winningConcepts = tmp[1];
      neCol.type = 'http://dbpedia.org/ontology/'+type;
      neCol.winnigConcepts = winningConcepts;
      weCols[neCol.index] = winningEntities;
      winningEntities = winningEntities.map(winningEntities => winningEntities.entity);
      if(debug) {
        console.log(type);
        console.log(winningEntities);
      }
    }
  });

  neCols = neCols.filter(item => !newNoAnnColsIndex.includes(item.index));

  InfoTable.update({_id: tableID}, {$set:{neCols: neCols}});

  TableDataCopy.update({_id: tableID}, {$set:{cols: cols}});
  
  Annotations.update(
    {_id:tableID},
    {
      _id:tableID,
      colsWE: weCols
    },
    {upsert: true}
  );

}

function getEntitiesAndDisambiguation(cols, indexes, colIndex, bwHeaderContext, winningEntities) {
  let column = cols[colIndex];
 
  for(let i=0; i<indexes.length; i++) {
    let index = indexes[i]
    let valueInCell = column[index].value;
    if(valueInCell == '')
      continue;
    let entities = sparqlGetEntities(column[index].valueForQuery);
    let resultEntities = [];
    let bwRowContext = getRowContext(cols, index, colIndex);
    let winningEntity = getWinningEntity(valueInCell, entities, bwHeaderContext, bwRowContext, resultEntities);
    cols[colIndex][index].resultEntities = resultEntities;
    let tmp = resultEntities.filter(item => item.entity == winningEntity)[0];
  
    if(winningEntity) 
      winningEntities.push(tmp);
  }
}

export function getWinningEntity(cell, entities, bwHeaderContext, bwRowContext, resultEntities) {
  let rowContextScoreMax = 1;
  let cellContextScoreMax = 1;

  for(let j=0; j<entities.length; j++) {
    [bowSetCell, bowSetEntity, bowSetAbstract] = getBowSet(cell, entities[j].s.value, entities[j].abstract.value);
    let entity = getNameFromEntity(entities[j].s.value);
    let scores = getScores(cell, entity, bowSetCell, bowSetEntity, bowSetAbstract, bwHeaderContext, bwRowContext, resultEntities);
    if(scores.rowContextScore > rowContextScoreMax)
      rowContextScoreMax = scores.rowContextScore;
    if(scores.cellContextScore > cellContextScoreMax)
      cellContextScoreMax = scores.cellContextScore;  
    resultEntities[j].entity = entities[j].s.value;
  }

  let winningEntity;
  let max = -Infinity;
  for(let j=0; j<resultEntities.length; j++) {
    let rowContextScoreNormalized = resultEntities[j].rowContextScore/rowContextScoreMax;
    let cellContextScoreNormalized = resultEntities[j].cellContextScore/cellContextScoreMax;
    let finalScore = rowContextScoreNormalized + cellContextScoreNormalized - resultEntities[j].editDistanceScore * 5;
    if(finalScore > max) {
      max = finalScore;
      winningEntity = resultEntities[j].entity;
    }
    resultEntities[j].rowContextScoreNormalized = rowContextScoreNormalized;
    resultEntities[j].cellContextScoreNormalized = cellContextScoreNormalized;
    resultEntities[j].finalScore = finalScore;
  }
  return winningEntity;
}

function checkIsNoAnnotation(winningEntities) {
  let count = 0;
  let weMap = new Map();
  for(let we of winningEntities) {
    if(!weMap.get(we.entity) && we.editDistanceScore < 0.4)
      count++;
    weMap.set(we.entity, true); 
  }
  return (count/weMap.size) < 0.50;
}

function getIndexesRows(tableRows) {
  let indexes = [];
  if(tableRows <= numberOfRows) {
    for(let i=0; i<tableRows; i++)
      indexes.push(i);
  }
  else {
    for(let i=0; i<10; i++)
      indexes.push(i);
    let tmp = Math.floor(tableRows/3) + 1;  
    for(let i=tmp; i<(tmp+10); i++)
      indexes.push(i);
    tmp = tableRows - 10;  
    for(let i=tmp; i<(tmp+10); i++)
      indexes.push(i);      
  }
  return indexes;
}

function getType(winningEntities, totalRowsTable, headerAttribute) {
  let globalConcepts = {};
  let maxType;
  let max = -Infinity;
  for(let e of winningEntities) {
    let types = sparqlGetType(e.entity);
    e.type = types.map(type => (type.type.value));
    types = extractTypes(types);
    let localConcepts = {};
    for(let type of types) {
      //type = type.split(/[0-9]+/)[0];
      if(ontologyClass.get(type)) {
        if(!globalConcepts[type]) {
          globalConcepts[type] = {};
          globalConcepts[type].frequency = 0;
          globalConcepts[type].row = 0; 
        }
        if(!localConcepts[type]) {
          localConcepts[type] = true;
          globalConcepts[type].row++;
        }
        globalConcepts[type].frequency++;
        if(globalConcepts[type].frequency > max) {
          max = globalConcepts[type].frequency;
          maxType = type;
        }
      }
    }
  }
  let winningConcepts = new Map();
  for(let key in globalConcepts) {
    let weightRows = (numberOfRows > totalRowsTable) ? totalRowsTable : numberOfRows; 
    if(globalConcepts[key].row/weightRows > 0.25 && globalConcepts[key].frequency/max > 0.25) {
      winningConcepts.set(key, globalConcepts[key].frequency);
    }
  }
  if(debug) {
    console.log(globalConcepts);
    console.log(winningConcepts);
  }
  let type = findPossibleConceptInHeader(winningConcepts, headerAttribute);
  type = type ? type : findPossibleSubType(maxType, winningConcepts);
  return [type, winningConcepts];
}

function extractTypes(types) {
  let str = '';
  for(let type of types) {
      let tmp = type.type.value;
      str += tmp.substring(tmp.lastIndexOf('/')+1, tmp.length) +  ' ';
  }
  str = str.trim();
  types = str.split(' ');
  return types;
}

function findPossibleConceptInHeader(types, header) {
  header = header.split(' ');
  let keys = [...types.keys()];
  let concept = [];
  for(let h of header) {
    h = h.charAt(0).toUpperCase() + h.substring(1, h.length);
    concept = concept.concat(keys.filter(concept => concept.includes(h)));
  }
  let type
  if(concept.length > 0)
    type = concept.reduce(function (a, b) { return a.length > b.length ? a : b; });
  return type ? type : undefined;
}

function findPossibleSubType(type, types) {
  while(true) {
    let substitution = false;
    types.forEach((val, key) => {
      if(ontologyGraph[type] && isMoreSubType(type, types))
        return type;
      if(isSubType(type, key)) {
        type = key;
        substitution = true;
      }
    });
    if(!substitution)
      break;
  }
  return type;
}

function isSubType(type1, type2) {
  if(!ontologyGraph[type1])
    return false;
  else if(ontologyGraph[type1][type2] || ontologyGraphCT[type1][type2])
    return true;
  else
    return false;  
}

function isMoreSubType(type, types) {
  let count = 0;
  types.forEach((val, key) => {
    if(ontologyGraph[type][key])
      count++;
  });
  return (count > 1);
}