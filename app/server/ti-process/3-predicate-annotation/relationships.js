import { sparqlGetLITrel, sparqlGetNErel } from '../utils/sparqlProxy';

export function getNErelationships(tableID) {
  let infoTable = InfoTable.findOne({_id:tableID});
  let neCols = infoTable.neCols;
  let subjectConcept = neCols.filter(item => item.index == infoTable.subCol)[0].type;
  let colsWE = Annotations.findOne({_id: tableID}).colsWE;
  let winningEntitiesSubCol = [...new Set(colsWE[infoTable.subCol].map(item => '<'+item.entity+'>'))].join(' ');

  for(let neCol of neCols) {
    if(neCol.index != infoTable.subCol) {
      let winningEntities = [...new Set(colsWE[neCol.index].map(item => '<'+item.entity+'>'))].join(' ');
      let result = sparqlGetNErel(winningEntities, winningEntitiesSubCol, subjectConcept, neCol.type);
      let predicate = getPredicateOfMaxFrequency(result);
      neCol.rel = predicate;
      console.log(predicate);
    }
  }

  let litCols = infoTable.litCols;
  
  for(let litCol of litCols) {
    let datatype = litCol.dataType.datatype.split(';')[0];
    let result = sparqlGetLITrel(winningEntitiesSubCol, subjectConcept, datatype);
    let predicate = getPredicateOfMaxFrequency(result);
    litCol.rel = predicate;
    console.log(predicate);
  }

  InfoTable.update({_id: tableID}, {$set:{neCols: neCols, litCols: litCols}});
}

function getPredicateOfMaxFrequency(result) {
  let max = -Infinity;
  let maxPredicate;
  for(let r of result) {
   let freq = parseInt(r.count.value);
    if(freq > max) {
      max = freq;
      maxPredicate = r.p.value;
    }
  }
  return maxPredicate;
}