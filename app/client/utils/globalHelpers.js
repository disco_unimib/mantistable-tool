import moment from 'moment';


Template.registerHelper('annotationIsNotComplete', function () {
  const t2dCompleted = InfoTable.find({ typeGS: 't2d', 'statusProcess.4.status': 'done' }).count();
  const t2dTot = InfoTable.find({ typeGS: 't2d' }).count();
  const limayeCompleted = InfoTable.find({ typeGS: 'limaye200', 'statusProcess.4.status': 'done' }).count();
  const limayeTot = InfoTable.find({ typeGS: 'limaye200' }).count();
  return t2dTot === 0 || limayeTot === 0
    || t2dCompleted !== t2dTot || limayeCompleted !== limayeTot;
});

Template.registerHelper('t2dIsLoaded', function () {
  return Tables.find({ typeGS: 'T2Dv2' }).count() >= 234;
});

Template.registerHelper('limayeIsLoaded', function () {
  return Tables.find({ typeGS: 'Limaye200' }).count() >= 200;
});

Template.registerHelper('isCurrentRoute', (currentRoute) => {
  let isCurrentRoute = false;
  currentRoute = currentRoute.split(';');
  $.each(currentRoute, function (index, value) {
    if (value === Router.current().route.getName()) {
      isCurrentRoute = true;
    }
  });

  return isCurrentRoute;
});

Template.registerHelper('isCurrentPath', currentPath => (Iron.Location.get().path).indexOf(currentPath) !== -1);

Template.registerHelper('offset', function (index) {
  return index + 1;
});

Template.registerHelper('formatDate', function (date) {
  return date !== undefined ? moment(date).format('DD/MM/YYYY - HH:mm:ss') : '-';
});

Template.registerHelper('and', (a, b) => a && b);

Template.registerHelper('or', (a, b) => a || b);

Template.registerHelper('orArray', (a, b) => {
  if (a !== undefined && b !== undefined) {
    return a.length > 0 || b.length > 0;
  }
  return false;
});

Template.registerHelper('offset', function (index) {
  return index + 1;
});

Template.registerHelper('percentage', function (value) {
  return parseInt(value * 100);
});

Template.registerHelper('hasTables', function () {
  return Tables.find().count() > 0;
});


Template.registerHelper('formatting', function (GS) {
  let typeGS;
  switch (GS) {
    case 'none':
      typeGS = '-';
      break;
    case 't2d':
    case 'T2Dv2':
      typeGS = 'T2D';
      break;
    case 'limaye200':
      typeGS = 'Limaye200';
      break;
    case 'CTAround1':
      typeGS = 'CTA Round 1';
      break;
    case 'CPAround1':
      typeGS = 'CPA Round 1';
      break;
    case 'CEAround1':
      typeGS = 'CEA Round 1';
      break;
    default:
      typeGS = GS;
  }
  return typeGS;
});
