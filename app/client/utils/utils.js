export function getScrollBarWidth() {
  const $outer = $('<div>').css({ visibility: 'hidden', width: 100, overflow: 'scroll' }).appendTo('body');


  const widthWithScroll = $('<div>').css({ width: '100%' }).appendTo($outer).outerWidth();
  $outer.remove();
  return 100 - widthWithScroll;
}

export function HandsontableWrapper(
  data = null, colHeaders = null, columns = null, cells = null, cell = null,
) {
  this.data = data;
  this.colHeaders = colHeaders;
  this.columns = columns;
  this.cells = cells;
  this.cell = cell;
  if (cell) {
    this.comments = true;
  } else {
    this.comments = false;
  }
}

// return true if current table (ident) has completed this process
export function isValidFile(ident, process) {
  let isValid = false;
  if (ident) {
    const statusProcesses = InfoTable.findOne({ _id: ident }).statusProcess;

    $.each(statusProcesses, function (index, value) {
      if (value.routeName === process) {
        if (value.status === 'done') {
          isValid = true;
        }
      }
    });
  }
  return isValid;
}

export function processStatus(ident, process) {
  let processStatus = '';

  if (ident) {
    const statusProcesses = InfoTable.findOne({ _id: ident }).statusProcess;

    $.each(statusProcesses, function (index, value) {
      if (value.routeName === process) {
        processStatus = value.status;
      }
    });
  }

  return processStatus;
}

export function processIsCurrentlyRunning(ident) {
  let isCurrentlyRunning = false;

  if (ident) {
    const statusProcesses = InfoTable.findOne({ _id: ident }).statusProcess;

    $.each(statusProcesses, function (index, value) {
      if (value.status === 'doing') {
        isCurrentlyRunning = true;
      }
    });
  }

  return isCurrentlyRunning;
}

export function runProcess(ident, process) {
  Meteor.call('updateProcessStatus', ident, process, 'doing');

  Meteor.call(process, ident);
}

export function getIcon(index) {
  const info = InfoTable.findOne({ _id: Template.instance().data._id });
  let type;

  for (key of info.litCols) {
    if (key.index === index) {
      type = key.type;
    }
  }

  let icon;

  switch (type) {
    case 'GeoCoordinates':
      icon = 'fas fa-globe-americas';
      break;
    case 'numeric':
      icon = 'fas fa-hashtag';
      break;
    case 'address':
      icon = 'fas fa-map-marker-alt';
      break;
    case 'hexColor':
      icon = 'fas fa-palette';
      break;
    case 'url':
    case 'fqdn':
      icon = 'fas fa-link';
      break;
    case 'creditcard':
      icon = 'far fa-credit-card';
      break;
    case 'email':
      icon = 'fas fa-envelope';
      break;
    case 'image':
      icon = 'fas fa-image';
      break;
    case 'ip':
      icon = 'fas fa-laptop';
      break;
    case 'isbn':
      icon = 'fas fa-book';
      break;
    case 'iso8601':
    case 'date':
      icon = 'fas fa-calendar';
      break;
    case 'boolean':
      icon = 'fas fa-toggle-on';
      break;
    case 'ID':
      icon = 'fas fa-key';
      break;
    case 'description':
      icon = 'fas fa-file-alt';
      break;
    case 'iata':
      icon = 'fas fa-plane';
      break;
    case 'currency':
      icon = 'fas fa-dollar-sign';
      break;
    default:
      icon = 'fas fa-ban';
      break;
  }

  return `<i class="${icon}" aria-hidden="true" title="${type}"></i> `;
}

export function buildColHeader(index, showmoreColType, objectData) {
  let colHeader = '';

  const hlabel = Object.keys(objectData[0]);

  const info = InfoTable.findOne({ _id: Template.instance().data._id });

  const SubCols = info.subCol;
  const neCols = [];
  const liCols = [];

  // get NE col
  if (info.neCols !== undefined) {
    for (key of info.neCols) {
      neCols.push(key.index);
    }
  }

  // get lit cols
  if (info.litCols !== undefined) {
    for (key of info.litCols) {
      liCols.push(key.index);
    }
  }

  // if literal -> get icon
  if (liCols.includes(index)) {
    colHeader += getIcon(index);
  }

  // get value
  colHeader += hlabel[index];

  // span for float right legend
  colHeader += '<span class="pull-right">';

  // s/ne/lit/na col legend
  if (index === SubCols) {
    colHeader += '<span class="legend s" title="Subject"> S </span>';
  } else if (neCols.includes(index)) {
    colHeader += '<span class="legend ne " title="Named Entity"> NE </span>';
  } else if (liCols.includes(index)) {
    colHeader += '<span class="legend l" title="Literal"> L </span>';
  } else {
    colHeader += '<span class="legend na" title="No Annotation"> NA </span>';
  }

  // show more button
  $.each(showmoreColType, function (i, value) {
    if (value === 'all'
      || (value === 'ne' && neCols.includes(index))
      || (value === 'lit' && liCols.includes(index))) {
      colHeader += ' <i title="more info" class="fa fa-info fa-fw show-more" aria-hidden="true"></i>';
    }
  });

  // close span
  colHeader += '</span>';

  return colHeader;
}

export function reRenderHandsontable() {
  if (typeof hot3 !== 'undefined') {
    setTimeout(function () {
      hot3.updateSettings({
        height: $('.sticky-footer').hasClass('open-console') ? $('.content').height() - 150 + 8 : $('.content')
          .height(),
        width: $('.content').width(),
      });
    }, 300);
  }
}

export function openRightSideBar() {
  $('body').addClass('control-sidebar-open');

  // recalc infobox in right sidebar width in order to hide scrollbar
  $('#infoBox').slimScroll({
    height: ' calc(100vh - 50px - 7px)',
  });


  reRenderHandsontable();
}

export function closeRightSideBar() {
  Session.set('clickedCol', undefined);
  Session.set('clickedRow', undefined);
  $('body').removeClass('control-sidebar-open');

  reRenderHandsontable();
}

export function openConsole() {
  if ($('.sticky-footer').hasClass('close-console')) {
    $('.sticky-footer').toggleClass('close-console');
    $('.sticky-footer').toggleClass('open-console');
    $('.up-down-arrow').toggleClass('fa-caret-up');
    $('.up-down-arrow').toggleClass('fa-caret-down');
  }

  $('.multiStep').css('margin-bottom', $('.sticky-footer').hasClass('open-console') ? '150px' : '');
}

export function hideConsole() {
  // close console if open and hide
  $('.sticky-footer').addClass('no-console');
  $('.sticky-footer').addClass('close-console');
  $('.sticky-footer').removeClass('open-console');
  $('.up-down-arrow').addClass('fa-caret-up');
  $('.up-down-arrow').removeClass('fa-caret-down');
  $('.content').addClass('noConsole');
}
