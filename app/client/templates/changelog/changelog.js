import { hideConsole } from '../../utils/utils';

Template.changelog.onRendered(function () {
  // $('body').addClass('hide-sidebar sidebar-collapse');
  hideConsole();

});

Template.changelog.onDestroyed(function () {
  // $('body').removeClass('hide-sidebar');
  $('.wrapper').css('height', 'auto');
  $('.wrapper').css('min-height', '100%');


  $('.sticky-footer').removeClass('no-console');
  $('.content').removeClass('noConsole');
});
