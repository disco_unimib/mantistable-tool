import Handsontable from 'handsontable';
import 'handsontable/dist/handsontable.css';
import { closeRightSideBar, openRightSideBar } from '../../../utils/utils';


/** ************************************************************************** */
/* displayHot: Event Handlers */
/** ************************************************************************** */
Template.displayHot.events({
  'mouseenter #HOT tr td'(event) {
    $(event.target).css('white-space', 'normal');
  },
  'mouseleave #HOT tr td'(event) {
    $(event.target).css('white-space', 'nowrap');
  },
});

/** ************************************************************************** */
/* displayHot: Helpers */
/** ************************************************************************** */
Template.displayHot.helpers({});

/** ************************************************************************** */
/* displayHot: Lifecycle Hooks */
/** ************************************************************************** */
Template.displayHot.onCreated(() => {
});

Template.displayHot.onRendered(() => {
  let contentChildrenHeight = 0;
  $('.content').children().each(function () {
    if ($(this).hasClass('removeFromPageHeight')) {
      contentChildrenHeight += $(this).outerHeight(true);
    }
  });

  // currentData should contain a HandsontableWrapper:
  const hotWrapper = Template.currentData();
  // not use cost/var/let to make hot3 globally accessible
  hot3 = new Handsontable(document.getElementById('HOT'), {
    data: hotWrapper.data,
    readOnly: true,
    minSpareRows: 0,
    sortIndicator: true,
    columnSorting: true,
    stretchH: 'all',
    preventOverflow: 'horizontal',
    selectionMode: 'single', // header(50)+footer(7)+padding(5+5)
    height: $(window).height() - 50 - 7 - 10 - contentChildrenHeight,
    colHeaders: hotWrapper.colHeaders,
    columns: hotWrapper.columns,
    cells: hotWrapper.cells,
    comments: hotWrapper.comments,
    cell: hotWrapper.cell,
  });

  // listener to mouse over cell.
  // default comment (textArea) is hidden and replaced with tmp div
  // in order to support html formatting
  Handsontable.hooks.add('beforeOnCellMouseOver', function (events, coord, td) {
    $('#divComment').remove();

    // hack to remove multiple instance of comments
    if ($('.htComments').length > 1) {
      $('.htCommentsContainer .htComments:not(:last-child)').remove();
    }

    const commentsPlugin = hot3.getPlugin('comments');

    if (commentsPlugin !== undefined) {
      const comment = commentsPlugin.getCommentAtCell(coord.row, coord.col);

      if (comment !== undefined || comment != null) {
        $('textarea.htCommentTextArea').hide();

        const divComment = `<div id='divComment' class='htCommentTextArea'>${comment}</div>`;

        $(divComment).appendTo('.htComments');
      }
    }
  });

  // listener to click on cell.
  Handsontable.hooks.add('afterOnCellMouseDown', function (event, coords) {
    hot3.setCellMeta(Session.get('clickedRow'), Session.get('clickedCol'), 'className', '');

    const sortedPlugin = hot3.getPlugin('columnSorting');

    const coordsCol = coords.col;
    let coordsRow;

    if (sortedPlugin !== 'undefined' && sortedPlugin.isSorted()) {
      coordsRow = hot3.runHooks('modifyRow', coords.row);
    } else {
      coordsRow = coords.row;
    }

    let validClick = false;

    // if a cell or a header with show info button
    // has been clicked
    if ($(event.srcElement).hasClass('htCommentCell')
      || (coords.row === -1 && hot3.getColHeader(coordsCol).indexOf('show-more') > -1)) {
      if (coords.row === -1) { // click on header
        // $('.show-more').on('click', function () {
        validClick = true;
        // });
      } else { // click on cell
        // if (Router.current().route.getName() !== 'columnsAnalysis') {
        validClick = true;
        // }
      }
    }

    if (validClick) {
      Session.set('clickedRow', coordsRow);
      Session.set('clickedCol', coordsCol);
      openRightSideBar();
    } else if (Router.current().route.getName() !== 'editAnnotations' && Router.current().route
      .getName() !== 'editTable') {
      closeRightSideBar();
    }
  });
});

Template.displayHot.onDestroyed(() => {

});
