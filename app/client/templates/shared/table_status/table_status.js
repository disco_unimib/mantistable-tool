import { Meteor } from 'meteor/meteor'
/** ************************************************************************** */
/* tableStatus: Event Handlers */
/** ************************************************************************** */
Template.tableStatus.events({});

/** ************************************************************************** */
/* tableStatus: Helpers */
/** ************************************************************************** */
Template.tableStatus.helpers({
    isEqual : function (a, b){
        return a === b;
    },
    processStatus() {

        return Template.currentData();
    }
});

/** ************************************************************************** */
/* tableStatus: Lifecycle Hooks */
/** ************************************************************************** */
Template.tableStatus.onCreated(() => {
});

Template.tableStatus.onRendered(() => {

});

Template.tableStatus.onDestroyed(() => {

});


