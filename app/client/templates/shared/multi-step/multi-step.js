import { openConsole, reRenderHandsontable, runProcess } from '../../../utils/utils';

Template.MultiStep.helpers({
  statusProcess() {
    let statusProcessCurrentTable = InfoTable.find({ _id: this._id });
    statusProcessCurrentTable = statusProcessCurrentTable.map(function (x) {
      return x.statusProcess;
    });

    return statusProcessCurrentTable[0];
  },
  isCurrentPage(page) {
    return Router.current().route.getName() === page ? 'active' : '';
  },
  isActive() {
    return Router.current().route.getName() === this.routeName ? 'active' : '';
  },
  nextStepTodo() {
    return this.status === 'todo';
  },
  isComplete() {
    const dataContext = this;
    const statusProcess = dataContext.infoTable.statusProcess;

    // return if last process is finished
    const annIsComplete = statusProcess[statusProcess.length - 1].status === 'done';

    return annIsComplete ? 'enabled' : 'disabled';
  },
  getID() {
    return { _id: Template.instance().data._id };
  },
});


Template.MultiStep.events({
  'click .multiStep .todo'(e) {
    e.preventDefault();
  },

  'click #preProcessing'(e) {
    e.stopPropagation();
    e.preventDefault();

    const ident = Template.instance().data._id;

    runProcess(ident, 'preProcessing');

    if (Router.current().route.getName() === 'startingTable') {
      Meteor.setTimeout(function () {
        Router.go('preProcessing', { _id: ident });
      }, 100);
    }
    openConsole();
  },
  'click #columnsAnalysis'(e) {
    e.stopPropagation();
    e.preventDefault();

    const ident = Template.instance().data._id;

    runProcess(ident, 'columnsAnalysis');


    if (Router.current().route.getName() === 'preProcessing') {
      Meteor.setTimeout(function () {
        Router.go('columnsAnalysis', { _id: ident });
      }, 100);
    }
    openConsole();
  },
  'click #conceptDatatypeAnnotation'(e) {
    e.stopPropagation();
    e.preventDefault();

    const ident = Template.instance().data._id;

    runProcess(ident, 'conceptDatatypeAnnotation');


    if (Router.current().route.getName() === 'columnsAnalysis') {
      Meteor.setTimeout(function () {
        Router.go('conceptDatatypeAnnotation', { _id: ident });
      }, 100);
    }

    openConsole();
  },
  'click #relationshipsAnnotation'(e) {
    e.stopPropagation();
    e.preventDefault();

    const ident = Template.instance().data._id;

    runProcess(ident, 'relationshipsAnnotation');

    if (Router.current().route.getName() === 'conceptDatatypeAnnotation') {
      Meteor.setTimeout(function () {
        Router.go('relationshipsAnnotation', { _id: ident });
      }, 100);
    }

    openConsole();
  },
  'click #entityLinking'(e) {
    e.stopPropagation();
    e.preventDefault();

    const ident = Template.instance().data._id;

    runProcess(ident, 'entityLinking');

    if (Router.current().route.getName() === 'relationshipsAnnotation') {
      Meteor.setTimeout(function () {
        Router.go('entityLinking', { _id: ident });
      }, 100);
    }

    // openConsole();
  },

  'click .editAnn.disabled a'(e) {
    e.preventDefault();
  },
});

Template.MultiStep.onRendered(() => {
  const sideBarMenuHeight = $('.sidebar-menu.tree').height();
  setTimeout(function () {
    $('.multiStepScrollbar').slimScroll({
      height: `calc(100vh - 100px - ${sideBarMenuHeight}px)`,
    });
  }, 500);
});