import Handsontable from 'handsontable';
import { HTTP } from 'meteor/http';
import { HandsontableWrapper } from '../../../utils/utils';

// update HOT to highlight columns
function updateSetting(clickedHoveredCol, subColIndex) {
  if (typeof hot3 !== 'undefined') {
    hot3.updateSettings({
      cells(row, col) {
        hot3.setCellMeta(row, col, 'className', 'opacity-low');

        if (col !== clickedHoveredCol && col !== subColIndex) {
          hot3.setCellMeta(this.instance.toVisualRow(row), col, 'className', 'opacity-low');
        } else {
          hot3.setCellMeta(row, col, 'className', 'test');
        }
      },
      afterGetColHeader(col, TH) {
        if (col !== clickedHoveredCol && col !== subColIndex) {
          Handsontable.dom.addClass(TH, 'opacity-low');
        } else {
          Handsontable.dom.removeClass(TH, 'opacity-low');
        }
      },
    });
  }
}

// reset HOT setting to remove highlighted columns
function resetSettings() {
  if (typeof hot3 !== 'undefined') {
    hot3.updateSettings({
      cells(row, col) {
        hot3.setCellMeta(row, col, 'className', '');
      },
      afterGetColHeader(col, TH) {
        Handsontable.dom.removeClass(TH, 'opacity-low');
      },
    });
  }
}

// prevent link style for anchor without href
function unstyleAnchor() {
  setTimeout(function () {
    $('#infoBox a').removeClass('noLink');

    $('#infoBox a').each(function () {
      if (!($(this).attr('href')).trim().startsWith('http')) {
        $(this).addClass('noLink');

        $(this).click(function (e) {
          e.preventDefault();
        });
      }
    });
  }, 100);
}

// collapse abstract if too long
function styleAbstract() {
  $('.abstract').removeClass('collapsed');
  $('.abstract .read-more').hide();

  setTimeout(function () {
    if ($('.abstract .text').height() > 100) {
      $('.abstract .read-more').show();
      $('.abstract').addClass('collapsed');
    }
  }, 100);
}

// remove schema from url; if url is undefined, return an alternative string
function removeUrlSchema(url, alternativeString) {
  if (url !== undefined && url !== '') {
    if (url.indexOf('XMLSchema#') > -1) {
      return `xsd:${url.toString().split('#').pop()}`;
    }
    return url.toString().split('/').pop();
  }
  return alternativeString;
}

// adjust column index to support extra columns
function colOffset(clickedCol) {
  if (clickedCol !== 0 && clickedCol % 2 === 0) {
    clickedCol /= 2;
  }
  return clickedCol;
}

// adjust prefix space
function prefixSpace() {
  setTimeout(function () {
    const prefixDbo = $('.material.prefix-dbo');
    $(prefixDbo).find('input').css('padding-left', `${$(prefixDbo).find('.prefix').outerWidth() + 3}px`);
  }, 50);
}

// get Subject column index
function getSubjectIndex() {
  const id = Router.current().params._id;
  return InfoTable.findOne({ _id: id }).subCol;
}

// check if argument is subject
function isSubject(column) {
  const id = Router.current().params._id;
  const subIndex = InfoTable.findOne({ _id: id }).subCol;
  return subIndex === column;
}

// check if argument is a literal column
function isLiteral(column) {
  const id = Router.current().params._id;
  const litCols = InfoTable.findOne({ _id: id }).litCols;

  let currentColLitType = [];
  const litColsIndex = [];

  // get Lit col
  $.each(litCols, function (index, value) {
    litColsIndex.push(value.index);
    if (value.index === column) {
      currentColLitType = value.colType;
    }
  });

  // Session for google pie chart
  Session.set('currentColLitType', currentColLitType);

  return litColsIndex.includes(column);
}

// check if argument is a NE column
function isNE(column) {
  const id = Router.current().params._id;
  const neCols = InfoTable.findOne({ _id: id }).neCols;

  const neColsIndex = [];

  // get Ne col
  $.each(neCols, function (index, value) {
    neColsIndex.push(value.index);
  });

  return neColsIndex.includes(column);
}

// get annotation for subject col
function getSubjectAnn() {
  const id = Router.current().params._id;
  const infoTable = InfoTable.findOne({ _id: id });
  const annotation = [];
  if (infoTable) {
    const neCols = infoTable.neCols;
    $.each(neCols, function (index, value) {
      if (isSubject(neCols[index].index)) {
        annotation.colName = value.header;
        annotation.type = value.type;
      }
    });
  }
  return annotation;
}

// get annotations for ne columns
function getNeAnn() {
  const id = Router.current().params._id;
  const infoTable = InfoTable.findOne({ _id: id });
  const annotation = [];
  if (infoTable) {
    const neCols = infoTable.neCols;
    $.each(neCols, function (index, value) {
      if (!isSubject(neCols[index].index)) {
        annotation.push({
          colName: value.header,
          colIndex: value.index,
          type: value.type,
          rel: value.rel,
        });
      }
    });
  }
  return annotation;
}

// get annotations for literal columns
function getLitAnn() {
  const id = Router.current().params._id;
  const infoTable = InfoTable.findOne({ _id: id });
  const annotation = [];
  if (infoTable) {
    const litCols = infoTable.litCols;
    $.each(litCols, function (index, value) {
      annotation.push({
        colName: value.header,
        colIndex: value.index,
        type: value.dataType,
        rel: value.rel,
      });
    });
  }
  return annotation;
}

// reset "save" button to initial state
function resetSaveButton() {
  const submitBtn = $('.submit-form');
  if (submitBtn.hasClass('click')) {
    submitBtn.removeClass('click');
    submitBtn.find('span').text('Save');
  }
}

// bold input match
function boldInputMatch(input) {
  // console.log($('ul.autocomplete').length);
  // $('ul.autocomplete li').each(function () {
  //   console.log($(this));
  //   const suggestion = $(this).find('.suggestion');
  //   suggestion.html(suggestion.text().replace(input, '<strong>$&</strong>'));
  // });
}

// call  from ABSTAT browse api
function callAbstat(queryParam) {
  Session.set('abstatError', undefined);
  const summary = '107dbfc4-0898-40fa-8e48-ab78992b0533';
  const enrichWithSPO = 'false';
  const limit = '10';
  const subtype = 'internal';

  let url = `${'http://backend.abstat.disco.unimib.it/api/v1/browse?'
  + 'enrichWithSPO='}${enrichWithSPO}&limit=${limit}&subtype=${subtype}&summary=${summary}&${queryParam}`;

  url = url.replace(/#/g, '%23');
  // console.log(url);

  HTTP.call('GET', url, function (error, result) {
    if (!error) {
      // if literal is Date, join result from Year query
      if (queryParam.indexOf('http://www.w3.org/2001/XMLSchema#date') > -1) {
        url = url.replace('%23date', '%23gYear');
        console.log(url);

        HTTP.call('GET', url, function (errorYear, resultYear) {
          if (!error) {
            Session.set('suggestions', $.extend(result.data.akps, resultYear.data.akps));
          } else {
            Session.set('abstatError', true);
          }
        });
      } else {
        Session.set('suggestions', result.data.akps);
      }
    } else {
      console.log(error);
      Session.set('suggestions', []);
      Session.set('abstatError', true);
    }
  });
}

// call ABSTAT suggestions api
function abstatSuggestion(type, input) {
  const limit = '7';
  const dataset = 'dbpedia-2015-10';

  const url = `${'http://backend.abstat.disco.unimib.it/api/v1/SolrSuggestions?'
  + 'qString='}${input}&qPosition=${type}&rows=${limit}&dataset=${dataset}`;

  // console.log(url);

  HTTP.call('GET', url, function (error, result) {
    if (!error) {
      Session.set('autocomplete', result.data.suggestions);
      boldInputMatch(input);
    } else {
      console.log(error);
    }
  });
}

// sidebarRight
Template.sidebarRight.onCreated(function () {
  Session.set('clickedRow', undefined);
  Session.set('clickedCol', undefined);
});

Template.sidebarRight.helpers({
  currentCellClicked() {
    if (this.datas !== undefined) {
      const objectData = this.datas.map(function (x) {
        return x.stringDoc;
      });

      if (Session.get('clickedCol') !== undefined
        && Session.get('clickedRow') !== undefined) {
        const clickedRow = Session.get('clickedRow');
        let clickedCol = Session.get('clickedCol');
        const currentCellClicked = {};

        // adjust colIndex in data prepraration step
        clickedCol = Router.current().route.getName() === 'preProcessing'
          ? colOffset(clickedCol) : clickedCol;

        currentCellClicked.clickedCol = clickedCol;
        currentCellClicked.clickedRow = clickedRow;
        currentCellClicked.cellHeader = Object.keys(objectData[0])[clickedCol];

        return currentCellClicked;
      }

      if (Router.current().route.getName() === 'relationshipsAnnotation'
        || Router.current().route.getName() === 'entityLinking') {
        resetSettings();
      }
    }
    return null;
  },
  step() {
    const info = Template.parentData().infoTable;
    const statusProcess = info.statusProcess;
    let step;

    $.each(statusProcess, function (index, value) {
      if (statusProcess[index].routeName === Router.current().route.getName()) {
        step = value.process;
      }
    });
    return step;
  },
});


// tableGeneralInfo
Template.tableGeneralInfo.helpers({
  isHeader() {
    const clickedCellContext = this;
    return clickedCellContext.clickedRow === -1;
  },
  cellTitle() {
    const clickedCellContext = this;
    let currentCellTitle;
    const isDataPrep = Router.current().route.getName() === 'preProcessing';

    if (clickedCellContext.clickedRow === -1) { // click on header
      currentCellTitle = clickedCellContext.cellHeader;
    } else { // click on cell
      currentCellTitle = hot3.getSourceDataAtCell(
        clickedCellContext.clickedRow,
        (isDataPrep ? colOffset(clickedCellContext.cellHeader) : clickedCellContext.clickedCol),
      );
    }
    return currentCellTitle;
  },
});


// dataPreparationSidebar
Template.dataPreparationSidebar.helpers({
  cellContent() {
    const clickedCellContext = this;
    const dataContext = Template.parentData();
    const cellContent = dataContext.tableDataCopy.cols[clickedCellContext.clickedCol][clickedCellContext.clickedRow];

    return cellContent;
  },
  transformations() {
    const clickedCellContext = this;
    const dataContext = Template.parentData();

    const transformations = dataContext.datas.map(function (x) {
      return x.transformations;
    });

    const transformationsByRow = transformations[clickedCellContext.clickedRow];
    const transformationsKey = Object.keys(transformations[0])[clickedCellContext.clickedCol];
    console.log(transformationsByRow[transformationsKey]);
    return [transformationsByRow[transformationsKey]];
  },
});


// columnsAnalysisSidebar
Template.columnsAnalysisSidebar.helpers({
  isHeader() {
    const clickedCellContext = this;
    return clickedCellContext.clickedRow === -1;
  },
  typeCol() {
    const clickedCellContext = this;
    let typeCol = 'No-Annotation';

    if (isNE(clickedCellContext.clickedCol)) {
      typeCol = 'Named Entity';
    } else if (isLiteral(clickedCellContext.clickedCol)) {
      typeCol = 'Literal';
    }
    return typeCol;
  },
  isLiteral() {
    const clickedCellContext = this;
    return isLiteral(clickedCellContext.clickedCol);
  },
  isNE() {
    const clickedCellContext = this;
    return isNE(clickedCellContext.clickedCol);
  },
  isSubject() {
    const clickedCellContext = Template.parentData();
    return isSubject(clickedCellContext.clickedCol);
  },
  NeInfo() {
    const dataContext = Template.parentData();
    const clickedCellContext = this;
    let score;

    $.each(dataContext.infoTable.neCols, function (index, value) {
      if (clickedCellContext.clickedCol === value.index) {
        score = value.score;
      }
    });

    return score;
  },
  column() {
    const dataContext = Template.parentData(2);
    const neInfoContext = this;

    const totalRow = dataContext.infoTable.stats.row;

    const column = {};

    column.emptyCells = Math.round((neInfoContext.emc / 10) * totalRow);
    column.totalRow = totalRow;

    return column;
  },
});


// conceptDatatypeAnnotationSidebar
Template.conceptDatatypeAnnotationSidebar.helpers({
  isHeader() {
    const clickedCellContext = this;
    return clickedCellContext.clickedRow === -1;
  },
  synonyms() {
    const clickedCellContext = this;
    const dictionary = Dictionary.findOne({ kw: clickedCellContext.cellHeader.toLowerCase() });
    let synonyms;
    if (dictionary !== undefined) {
      synonyms = (dictionary.syn).replace(/,[A-Za-z]/g, ', ').replace(/,\s*$/, '');
    }
    return synonyms;
  },
  winningConceptOrDatatype() {
    const clickedCellContext = this;
    const dataContext = Template.parentData();
    const neCols = dataContext.infoTable.neCols;
    const litCols = dataContext.infoTable.litCols;
    const currentColWinningConcept = [];
    const winningConcept = [];
    // let winningConceptTitle;
    let type;

    if (isNE(clickedCellContext.clickedCol)) {
      $.each(neCols, function (index, value) {
        if (value.index === clickedCellContext.clickedCol) {
          winningConcept.uri = value.type;
        }
      });
      winningConcept.title = removeUrlSchema(winningConcept.uri, 'not annotation');
      type = 'Winning concept';
    } else if (isLiteral(clickedCellContext.clickedCol)) {
      $.each(litCols, function (index, value) {
        let dataType;
        if (value.index === clickedCellContext.clickedCol) {
          dataType = value.dataType;
          winningConcept.uri = dataType.datatypeUrl;
          winningConcept.title = dataType.datatype;
        }
      });

      type = 'Datatype';
    }

    const wcUriSplit = winningConcept.uri.split(';');
    const wcTitleSplit = winningConcept.title.split(';');
    $.each(wcTitleSplit, function (index) {
      currentColWinningConcept.push({
        type,
        uri: wcUriSplit[index],
        title: wcTitleSplit[index],
      });
    });

    return currentColWinningConcept;
  },
  abstract() {
    const clickedCellContext = this;
    const dataContext = Template.parentData();
    let abstract;

    const cellContent = hot3.getSourceDataAtCell(
      clickedCellContext.clickedRow, clickedCellContext.clickedCol,
    );

    dataContext.annotations.forEach(function (x) {
      if (x.cellContent === cellContent) {
        abstract = x.entities[0].abstr;
      }
    });

    styleAbstract();
    return abstract;
  },
  entities() {
    const clickedCellContext = this;
    const dataContext = Template.parentData();
    let entitiesDB = {};
    const entities = [];
    let winningEntity;

    const cellContent = hot3.getSourceDataAtCell(
      clickedCellContext.clickedRow, clickedCellContext.clickedCol,
    );
    // get data from DB
    dataContext.annotations.forEach(function (x) {
      if (x.cellContent === cellContent) {
        entitiesDB = x.entities[0];
        winningEntity = x.winningEntity[0].uri;
      }
    });

    // create array for template
    $.each(entitiesDB.keys, function (index, value) {
      entities.push({
        name: value.split('/').pop(),
        uri: value,
        scoreEC: entitiesDB.scoreEC[index],
        scoreEN: entitiesDB.scoreEN[index],
        finalScore: entitiesDB.values[index],
        isWinning: winningEntity === value,
      });
    });

    return entities;
  },
  concepts() {
    const clickedCellContext = this;
    const dataContext = Template.parentData();
    let conceptsDB = {};
    const concepts = [];
    let winningConcept;

    const cellContent = hot3.getSourceDataAtCell(
      clickedCellContext.clickedRow, clickedCellContext.clickedCol,
    );
    // get data from DB
    dataContext.annotations.forEach(function (x) {
      if (x.cellContent === cellContent) {
        conceptsDB = x.entities[1];
        winningConcept = removeUrlSchema(x.winningConcept[0].uri);
      }
    });
    // create array for template
    $.each(conceptsDB.concetti, function (index, value) {
      concepts.push({
        name: value,
        finalScore: conceptsDB.score[index],
        isWinning: winningConcept === value,
      });
    });

    return concepts;
  },
});

Template.conceptDatatypeAnnotationSidebar.events({
  'click .abstract .read-more'() {
    $('.abstract').toggleClass('collapsed');

    if ($('.abstract').hasClass('collapsed')) {
      $('.abstract .read-more').text('Show more');
    } else {
      $('.abstract .read-more').text('Show less');
    }
  },
  'click #infoBox .nav-tabs li'() {
    styleAbstract();
  },
});


// relationshipsAnnotationSidebar
Template.relationshipsAnnotationSidebar.helpers({
  isSubject() {
    const clickedCellContext = this;
    return isSubject(clickedCellContext.clickedCol);
  },
  relationship() {
    const dataContext = Template.parentData();
    const clickedCellContext = this;

    let relationship = [];
    const allRelationships = [];

    const subjectAnn = getSubjectAnn();
    const neColsAnn = getNeAnn();
    const litColsAnn = getLitAnn();

    $.each(neColsAnn.concat(litColsAnn), function (index, value) {
      allRelationships.push({
        currentCol: value.colIndex,
        subject: {
          col: subjectAnn.colName,
          type: removeUrlSchema(subjectAnn.type, 'no annotation'),
          uri: subjectAnn.type,
        },
        object: {
          col: value.colName,
          type: isNE(value.colIndex) ? removeUrlSchema(value.type, 'no annotation')
            .split(';') : value.type.datatype.split(';'),
          uri: isNE(value.colIndex) ? value.type.split(';') : value.type.datatypeUrl.split(';'),
        },
        predicate: {
          type: removeUrlSchema(value.rel, 'not found'),
          uri: value.rel,
        },
      });
    });

    if (isSubject(clickedCellContext.clickedCol)) {
      resetSettings();
      relationship = allRelationships;
    } else {
      updateSetting(clickedCellContext.clickedCol, dataContext.infoTable.subCol);
      $.each(allRelationships, function (index, value) {
        if (value.currentCol === clickedCellContext.clickedCol) {
          relationship.push(value);
        }
      });
    }

    unstyleAnchor();

    return relationship;
  },
  getObjectUri(index) {
    return Template.parentData().object.uri[index];
  },
});

Template.relationshipsAnnotationSidebar.events({
  'mouseenter .highlightCol tbody tr'(event) {
    const infoTable = Template.parentData().infoTable;
    const subColIndex = infoTable.subCol;
    updateSetting($(event.target).data('col'), subColIndex);
  },
  'mouseleave  .highlightCol tbody tr'() {
    resetSettings();
  },
});


// pieChart
Template.pieChart.onRendered(function () {
  this.autorun(() => {
    const colorsPaletteMonochrome = ['#00a65a', '#128059', '#255A58', '#2F4858', '#4A496E', '#734C90'];
    const currentColLitType = Session.get('currentColLitType');

    $(currentColLitType).each(function () {
      if (this.name === '') {
        this.name = 'no type identified';
      }
    });

    const dataset = [];
    const colors = [];
    const slices = {};


    let index = 0;
    if (currentColLitType !== undefined) {
      currentColLitType.forEach(function (e) {
        dataset.push([e.name, e.rate * 100]);

        if (e.isMax) {
          slices[1] = { offset: 0.5 };
        }

        if (colorsPaletteMonochrome.length < index) {
          index = 0;
        }

        colors.push(colorsPaletteMonochrome[index++]);
      });

      dataset.sort(function (a, b) {
        return b[1] - a[1];
      });
      dataset.unshift(['LiteralType', 'Percentage']);

      // Load the Visualization API and the corechart package.
      google.charts.load('current', { packages: ['corechart'] });

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);
    }


    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChart() {
      const data = google.visualization.arrayToDataTable(dataset);

      // Set chart options
      const options = {
        width: '320',
        height: '400',
        chartArea: { width: '320', top: '10', left: '10' },
        pieSliceText: 'label',
        pieSliceTextStyle: { fontSize: '13' },
        legend: { alignment: 'center', position: 'right', textStyle: { fontSize: '13' } },
        tooltip: { showColorCode: true, text: 'percentage' },
        colors,
      };


      // Instantiate and draw our chart, passing in some options.
      const chart = new google.visualization.PieChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
  });
});


// editAnnotationsSidebar
Template.editAnnotationsSidebar.helpers({
  relationship() {
    const allRelationships = [];

    const subjectAnn = getSubjectAnn();
    const neColsAnn = getNeAnn();
    const litColsAnn = getLitAnn();

    $.each(neColsAnn.concat(litColsAnn), function (index, value) {
      allRelationships.push({
        currentCol: value.colIndex,
        subject: {
          col: subjectAnn.colName,
          type: removeUrlSchema(subjectAnn.type, 'no annotation'),
          uri: subjectAnn.type,
        },
        object: {
          col: value.colName,
          type: isNE(value.colIndex) ? removeUrlSchema(value.type, 'no annotation')
            .split(';') : value.type.datatype.split(';'),
          uri: isNE(value.colIndex) ? value.type.split(';') : value.type.datatypeUrl.split(';'),
        },
        predicate: {
          type: removeUrlSchema(value.rel, 'not found'),
          uri: value.rel,
        },
      });
    });
    unstyleAnchor();
    return allRelationships;
  },
  getObjectUri(index) {
    return Template.parentData().object.uri[index];
  },
  isNE() {
    const currentAnnotationContext = this;
    const relationshipsContext = Template.parentData();
    const dataContext = Template.parentData(2);

    let colIndex;
    if (currentAnnotationContext.colType === 'subject') {
      colIndex = dataContext.infoTable.subCol;
    } else if (currentAnnotationContext.colType === 'object') {
      colIndex = relationshipsContext.currentCol;
    }

    return isNE(colIndex);
  },
  isLiteral() {
    const currentAnnotationContext = this;
    const relationshipsContext = Template.parentData();
    if (currentAnnotationContext.colType === 'object') {
      return isLiteral(relationshipsContext.currentCol);
    }
    return null;
  },
  isPredicate() {
    const currentAnnotationContext = this;
    return currentAnnotationContext.colType === 'predicate';
  },
  chosenAnnToEdit() {
    const currentRelRow = this;
    let choosenAnnToEdit;
    let type;
    let uri;
    const typeColToEdit = Session.get('typeColToEdit');

    if (typeColToEdit && currentRelRow.currentCol === Session.get('editCol')) {
      switch (typeColToEdit) {
        case 'subject':
        default:
          type = currentRelRow.subject.type;
          break;
        case 'object':
          type = currentRelRow.object.type;
          break;
        case 'predicate':
          type = currentRelRow.predicate.type;
          break;
      }

      if (type === 'not found' || type === 'no annotation') {
        type = '';
      }

      choosenAnnToEdit = {
        type,
        colType: typeColToEdit !== undefined ? typeColToEdit : 'subject',
      };
    }
    return choosenAnnToEdit;
  },
  AbstatSuggestions() {
    const currentAnnotationContext = this;
    const relationshipsContext = Template.parentData();
    const clickedCellContext = Template.parentData(1);
    const suggestions = [];

    // call ABSTAT only if a edit module is open
    if (currentAnnotationContext.colType === Session.get('typeColToEdit')
      && Session.get('suggestions') === undefined) {
      switch (currentAnnotationContext.colType) {
        case 'subject':
        default:
          callAbstat(`pred=${relationshipsContext.predicate.uri}&obj=${relationshipsContext.object.uri[0]}`);
          break;
        case 'object':
          callAbstat(`subj=${relationshipsContext.subject.uri}&pred=${relationshipsContext.predicate.uri}`);
          break;
        case 'predicate':
          callAbstat(`subj=${relationshipsContext.subject.uri}&obj=${relationshipsContext.object.uri[0]}`);
          break;
      }
    }

    // return only suggestions for selected element
    $.each(Session.get('suggestions'), function (index, value) {
      switch (currentAnnotationContext.colType) {
        case 'subject':
        default:
          suggestions.push({
            globalURL: value.subject,
            globalFrequency: value.frequency,
          });
          break;
        case 'object':
          // if is gYear change with date
          if (value.object.indexOf('http://www.w3.org/2001/XMLSchema#gYear') > -1) {
            value.object = value.object.replace('XMLSchema#gYear', 'XMLSchema#date');
          }
          // if isNE or (isLiteral &&) suggestion is one of select option
          if (isNE(clickedCellContext.currentCol)
            || $(`select option[value="${removeUrlSchema(value.object)}"]`).length > 0) {
            suggestions.push({
              globalURL: value.object,
              globalFrequency: value.frequency,
            });
          }
          break;
        case 'predicate':
          suggestions.push({
            globalURL: value.predicate,
            globalFrequency: value.frequency,
          });
          break;
      }
    });

    if (Session.get('suggestions') !== undefined) {
      Session.set('isLoadingSuggestions', false);
    }

    return suggestions;
  },
  AbstatReturnMessage() {
    if (Session.get('abstatError')) {
      return '<span class="red">Service error. Suggestions can not be retrieved.</span>';
    }
    return 'No suggestion found.';
  },
  globalURL() {
    if (this.globalURL.indexOf('http://www.w3.org/2001/XMLSchema#gYear') > -1) {
      return this.globalURL.replace('XMLSchema#gYear', 'XMLSchema#date');
    }
    return this.globalURL;
  },
  MantisSuggestions() {
    const currentAnnotationContext = this;
    const relationshipsContext = Template.parentData();
    const dataContext = Template.parentData(2);

    const colIndex = currentAnnotationContext.colType === 'subject'
      ? relationshipsContext.subject.col : relationshipsContext.object.col;

    let suggestions = [];
    dataContext.annotations.forEach(function (x) {
      if (colIndex === x.Col && x.entities) {
        console.log(x.Col);

        $.each(x.entities[1].concetti, function (index, value) {
          suggestions.push(value);
        });
      }
    });

    suggestions = _.uniq(suggestions);
    return suggestions;
  },
  datatype() {
    const datatype = [
      'xsd:string',
      'xsd:double',
      'xsd:float',
      'xsd:integer',
      // 'xsd:nonPositiveInteger',
      // 'xsd:positiveInteger',
      // 'xsd:nonNegativeInteger',
      // 'xsd:negativeInteger',
      'xsd:boolean',
      'xsd:anyURI',
      'xsd:date',
    ];
    return datatype;
  },
  isDBDatatype() {
    const datatypeContext = this;
    const choosenAnnToEditContext = Template.parentData();
    return datatypeContext.toString() === choosenAnnToEditContext.type[0];
  },
  removeSchema(url) {
    return removeUrlSchema(url);
  },
  isLoadingSuggestions() {
    return Session.get('isLoadingSuggestions');
  },
  autocomplete() {
    const autocomplete = Session.get('autocomplete');
    if (autocomplete && autocomplete.length > 0) {
      $('ul.autocomplete').addClass('isVisible');
    } else {
      $('ul.autocomplete').removeClass('isVisible');
    }
    return autocomplete;
  },
  icon(colType) {
    let typeList;

    switch (colType) {
      case 'subject':
        typeList = [
          {
            type: 'subject',
            icon: 'fa-lock-open',
            status: 'active',
          },
          {
            type: 'predicate',
            icon: 'fa-lock',
          },
          {
            type: 'object',
            icon: 'fa-lock',
          },
        ];
        break;
      case 'object':
        typeList = [
          {
            type: 'subject',
            icon: 'fa-lock',

          },
          {
            type: 'predicate',
            icon: 'fa-lock',
          },
          {
            type: 'object',
            icon: 'fa-lock-open',
            status: 'active',
          },
        ];
        break;
      case 'predicate':
        typeList = [
          {
            type: 'subject',
            icon: 'fa-lock',

          },
          {
            type: 'predicate',
            icon: 'fa-lock-open',
            status: 'active',
          },
          {
            type: 'object',
            icon: 'fa-lock',
          },
        ];
        break;
      default:
        break;
    }

    return typeList;
  },
});

Template.editAnnotationsSidebar.events({
  'click .edit.disabled'(event) {
    event.preventDefault();
  },

  'click .edit:not(.disabled):not(.edit-close)'(event) {
    const currentRow = $(event.currentTarget).closest('tr');
    const editPanelRow = $(currentRow).next('tr');
    const editPanel = $(editPanelRow).find('.edit-panel');

    // reset previous suggestions
    Session.set('isLoadingSuggestions', true);

    // set session to target col
    Session.set('editCol', $(event.currentTarget).closest('tr').data('col'));
    Session.set('typeColToEdit', 'subject');

    // open edit form
    editPanelRow.toggleClass('hiddenRow showRow');
    editPanel.show(300, function () {
      // focus input
      $('input').focus();
    });

    // add class to current row
    currentRow.addClass('edit-mode');

    // change current edit button in close button
    $(event.currentTarget).addClass('edit-close');
    $(event.currentTarget).find('i').toggleClass('fa-window-close fa-pen-square');

    // disable other row edit button
    $('.edit:not(.edit-close)').addClass('disabled');

    // reduce opacity of other row
    $('.relationshipsTable tbody tr').each(function () {
      if (!$(this).is($(currentRow)) && !$(this).hasClass('showRow')) {
        $(this).addClass('opacity-low');
      }
    });

    // set active to subject pencil icon
    $(currentRow).find('.subject .td-edit').addClass('active');

    // adjust prefix space in input
    prefixSpace();

    // highlight column in HOT table
    updateSetting($(event.currentTarget).closest('tr').data('col'), getSubjectIndex());
  },

  'click .td-edit'(event) {
    // reset previous suggestions
    Session.set('suggestions', undefined);
    Session.set('isLoadingSuggestions', true);
    Session.set('autocomplete', undefined);

    // focus input
    $('input').focus();

    const currentTd = $(event.currentTarget).closest('td');

    // Set active class to clicked icon
    $('.td-edit').removeClass('active');
    $(event.currentTarget).addClass('active');

    // session for selected col type (subject, predicate, object)
    Session.set('typeColToEdit', $(currentTd).attr('class'));

    // adjust prefix space in input
    prefixSpace();

    // reset "save" button
    resetSaveButton();
  },

  'click .edit-close'(event) {
    // reset session
    Session.set('editCol', undefined);
    Session.set('typeColToEdit', undefined);
    Session.set('suggestions', undefined);
    Session.set('isLoadingSuggestions', true);
    Session.set('autocomplete', undefined);


    const currentRow = $(event.currentTarget).closest('tr');
    const editPanelRow = $(currentRow).next('tr');
    const editPanel = $(editPanelRow).find('.edit-panel');

    // close edit form
    editPanel.hide(300, function () {
      editPanelRow.toggleClass('hiddenRow showRow');
    });
    currentRow.removeClass('edit-mode');

    // change current close button in edit button
    $(event.currentTarget).removeClass('edit-close');
    $(event.currentTarget).find('i').toggleClass('fa-window-close fa-pen-square');

    // Remove active class to pen icon
    $('.td-edit').removeClass('active');

    // enable row edit button
    $('.edit').removeClass('disabled');

    // remove opacity-low
    $('.relationshipsTable tbody tr').removeClass('opacity-low ');

    // reset HOT settings
    resetSettings();
  },

  'click .reset-form'(event) {
    const editPanel = $(event.currentTarget).closest('.edit-panel');

    $(editPanel).find('input').each(function () {
      $(this).val($(this).data('initial-value'));
    });

    $(editPanel).find('select').each(function () {
      const initialValue = $(this).data('initial-value').split(',')[0];
      $(this).find('option').prop('selected', false);
      $(this).find(`option[value="${initialValue}"]`).prop('selected', true);
    });
  },

  'click .submit-form'(event) {
    event.stopPropagation();
    event.preventDefault();

    // animate send button
    $(event.currentTarget).addClass('click');
    $(event.currentTarget).find('span').text('Saved');


    // get table id
    const id = Router.current().params._id;

    // get editedTypeCol
    const editedTypeCol = Session.get('typeColToEdit');

    // get col Index
    const colIndex = $(event.currentTarget).closest('tr').data('col');

    let newAnnotation;
    if ($('input').length > 0) {
      newAnnotation = $('input').val();
      if (newAnnotation.trim() !== '') {
        newAnnotation = $('input').closest('.form-group').find('.prefix').data('prefix') + newAnnotation;
      }
    }

    if ($('select').length > 0) {
      newAnnotation = $('select').val();
    }

    switch (editedTypeCol) {
      case 'subject':
        Meteor.call('updateNeAnn', id, getSubjectIndex(), newAnnotation);
        break;
      case 'object':
        if (isNE(colIndex)) {
          Meteor.call('updateNeAnn', id, colIndex, newAnnotation);
        } else {
          Meteor.call('updateLitAnn', id, colIndex, newAnnotation);
        }
        break;
      case 'predicate':
        Meteor.call('updatePredicateAnn', id, colIndex, newAnnotation);
        break;
      default:
        break;
    }
  },

  'keydown input, change select'(e) {
    resetSaveButton();
  },

  'focus input'(event) {
    // prevent submit form with enter
    return event.which !== 13;
  },

  // when input lost focus
  'focusout input'(event) {
    $('ul.autocomplete')
      .removeClass('isVisible')
      .find('li').removeClass('active');
  },

  // when typing in the input
  'keyup input'(event) {
    // get editedTypeCol
    const editedTypeCol = Session.get('typeColToEdit');

    const input = $('input');

    if (input.data('prev-value') === undefined) {
      input.data('prev-value', input.data('initial-value'));
    }

    let typeCol;
    switch (editedTypeCol) {
      case 'subject':
        typeCol = 'subj';
        break;
      case 'object':
        typeCol = 'obj';
        break;
      case 'predicate':
        typeCol = 'pred';
        break;
      default:
        break;
    }

    if (input.data('prev-value').trim() !== input.val().trim()) {
      abstatSuggestion(typeCol, input.val());
    }

    input.data('prev-value', input.val());
  },

  // press up/down key arrow
  'keydown'(event) {
    const autocompleteDiv = $('.autocomplete');
    if (autocompleteDiv.hasClass('isVisible')) {
      const firstLi = autocompleteDiv.find('li:first-child');
      const lastLi = autocompleteDiv.find('li:last-child');
      const liActive = autocompleteDiv.find('li.active');
      const liNext = liActive.next();
      const liPrev = liActive.prev();

      switch (event.key) {
        case 'ArrowDown':
          // if an active li does not exist
          if (liActive.length === 0) {
            // add active to first li
            firstLi.addClass('active');
          } else {
            // remove active from li
            liActive.removeClass('active');
            // if active li ha next
            if (liNext.length > 0) {
              // add active to next
              liNext.addClass('active');
            } else {
              // add active to first li
              firstLi.addClass('active');
            }
          }
          break;
        case 'ArrowUp':
          // if an active li does not exist
          if (liActive.length === 0) {
            // add active to last li
            lastLi.addClass('active');
          } else {
            // remove active from li
            liActive.removeClass('active');
            // if active li ha prev
            if (liPrev.length > 0) {
              // add active to prev
              liPrev.addClass('active');
            } else {
              // add active to last li
              lastLi.addClass('active');
            }
          }
          break;
        case 'Enter':
          $('input').val(liActive.find('.suggestion').text().trim());
          $('input').blur();
          break;
        default:
          break;
      }
    }
  },

  // mouse over li
  'mouseenter ul.autocomplete li'(event) {
    $('ul.autocomplete li').removeClass('active');
    $(event.currentTarget).addClass('active');
  },

  'click ul.autocomplete li'(event) {
    event.preventDefault();
    const autocomplete = $(event.currentTarget).find('.suggestion').text().trim();
    $('input').val(autocomplete);
    $('ul.autocomplete').removeClass('isVisible');
    resetSaveButton();
  },

  // focus/unfocus ul.autocomplete when mouse enter/leave
  'mouseenter ul.autocomplete'(event) {
    $(event.currentTarget).addClass('focus');
  },
  'mouseleave ul.autocomplete'(event) {
    $(event.currentTarget).removeClass('focus');
  },

  'click .use-hint'(event) {
    event.preventDefault();
    const hint = $(event.currentTarget)
      .parent()
      .find('a')
      .text()
      .trim();

    if ($('input').length > 0) {
      $('input').val(hint);
    }

    if ($('select').length > 0) {
      $('select').val(hint);
    }
    resetSaveButton();
  },

});

Template.editAnnotationsSidebar.onDestroyed(function () {
  // reset session
  Session.set('editCol', undefined);
  Session.set('typeColToEdit', undefined);
  Session.set('suggestions', undefined);
  Session.set('autocomplete', undefined);
});

Template.editAnnotationsSidebar.onRendered(function () {
  // reset session
  Session.set('editCol', undefined);
  Session.set('typeColToEdit', undefined);
  Session.set('suggestions', undefined);
  Session.set('autocomplete', undefined);

  // setTimeout(function () {
  //   this.$('[data-toggle="tooltip"]').tooltip({ placement: 'bottom' });
  // }, 5000);
});


// editTableInfoSidebar
Template.editTableInfoSidebar.helpers({
  unvalidfile() {
    return Session.get('unvalidfile');
  },
});

Template.editTableInfoSidebar.events({
  'click .reset-form '(e, t) {
    e.stopPropagation();
    e.preventDefault();

    $('input').each(function () {
      $(this).val($(this).data('initial-value'));
    });

    if ($('#file').files) {
      $('#file').files[0] = null;
    }
  },
  'submit form'(e, t) {
    e.stopPropagation();
    e.preventDefault();
    const ident = Router.current().params._id;
    {
      const name = e.target.name.value;
      if (t.find('#file').files[0] == null) {
        Session.set('uploadingTable', true);
        Meteor.call('simpleUpdate', name, ident,
          function (error, result) {
            if (result) {
              Session.set('uploadingTable', false);
              Session.set('editing', false);
            }
          });
      } else {
        const file = t.find('#file').files[0];
        if (!/.*\.json/i.test(file.name)) {
          Session.set('unvalidfile', true);
          t.find('#file').files[0] = null;
          e.target.file.value = null;
        } else {
          const reader = new FileReader();
          reader.onload = function () {
            Session.set('uploadingTable', true);
            Meteor.call('updateJson', file.name, reader.result, name, ident,
              function (error, result) {
                if (result) {
                  t.find('#file').files[0] = null;
                  e.target.file.value = null;
                  Session.set('unvalidfile', false);
                  Session.set('uploadingTable', false);
                  Session.set('editing', false);
                  // EditTable.handsontablewrapper();
                }
              });
          };
          reader.onerror = function (error) {
            alert(error);
          };
          reader.readAsText(file);
        }
      }
    }
  },
  // remove error class from input
  'change input'(e) {
    Session.set('unvalidfile', false);
  },
});
