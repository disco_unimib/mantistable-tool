import { openConsole } from '../../../utils/utils';

Template.CreateTables.events({
  'submit form'(e, t) {
    e.stopPropagation();
    e.preventDefault();

    openConsole();

    const name = e.target.name.value;
    const file = t.find('#file').files[0];
    const goldStandard = $('#goldStandard').val();
    if (!/.*\.json/i.test(file.name)) {
      Session.set('unvalidfile', true);
      t.find('#file').files[0] = null;
      e.target.file.value = null;
    } else {
      const reader = new FileReader();
      reader.onload = function () {
        Session.set('update_start', true);
        Meteor.call('file-upload', file.name, reader.result, name, goldStandard,
          function (error, result) {
            if (result) {
              e.target.name.value = null;
              t.find('#file').files[0] = null;
              e.target.file.value = null;
              $('#goldStandard').find('option').attr('selected', false);
              Session.set('validfile', true);
              Session.set('unvalidfile', false);
            }
          });
      };
      reader.onerror = function (error) {
        alert(error);
      };
      reader.readAsText(file);
    }
  },
  // remove error class from input
  'change input'(e) {
    Session.set('unvalidfile', false);
  },

  'click .load_T2D_tables'(event) {
    event.stopPropagation();
    event.preventDefault();

    Session.set('isLoadingGST2D', true);

    Meteor.call('loadTables', 'T2Dv2', (err, res) => {
      Session.set('isLoadingGST2D', false);
    });
  },

  'click .load_limaye200_tables'(event) {
    event.stopPropagation();
    event.preventDefault();

    Session.set('isLoadingGSLimaye200', true);

    Meteor.call('loadTables', 'Limaye200', (err, res) => {
      Session.set('isLoadingGSLimaye200', false);
    });
  },


  'click .load_round2_tables'(event) {
    event.stopPropagation();
    event.preventDefault();

    Session.set('isLoadingGSRound2', true);

    Meteor.call('loadTables', 'Round2', (err, res) => {
      Session.set('isLoadingGSRound2', false);
    });
  },

  'click .delete_T2D_tables'(event) {
    event.stopPropagation();
    event.preventDefault();
    Session.set('isLoadingGST2D', true);
    Meteor.call('deleteTables', 't2d', (err, res) => {
      Session.set('isLoadingGST2D', false);
    });
  },

  'click .delete_limaye200_tables'(event) {
    event.stopPropagation();
    event.preventDefault();
    Session.set('isLoadingGSLimaye200', true);
    Meteor.call('deleteTables', 'limaye200', (err, res) => {
      Session.set('isLoadingGSLimaye200', false);
    });
  },
});

Template.CreateTables.helpers({
  validfile() {
    return Session.get('validfile');
  },
  unvalidfile() {
    return Session.get('unvalidfile');
  },
  update_start() {
    return Session.get('update_start');
  },
  logs() {
    return Alerts.find();
  },
  isLoadingGS(GS) {
    return Session.get(`isLoadingGS${GS}`);
  },
});

Template.CreateTables.onCreated(function () {
  Session.set('templateLoaded', true);
});

Template.CreateTables.onRendered(function () {
  Session.set('currentPageHeader', 'Create Table');
  Session.set('validfile', false);
  Session.set('unvalidfile', false);
  Session.set('update_start', false);

  $('.content').addClass('noFixedHeight');
});

Template.CreateTables.onDestroyed(function () {
  Session.set('templateLoaded', false);
  $('.content').removeClass('noFixedHeight');
});
