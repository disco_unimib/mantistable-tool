import { HandsontableWrapper, isValidFile, processStatus } from '../../../../utils/utils';

Template.PreProcessing.helpers({
  handsontablewrapper_notprocessed() {
    const jsonFileCurrent = this.jsonFile;
    return new HandsontableWrapper(
      JSON.parse(jsonFileCurrent.data),
      Object.keys(JSON.parse(jsonFileCurrent.data)[0]),
    );
  },
  handsontablewrapper_processed() {
    const tdc = this.tableDataCopy;
    let data = [];
    let cols = tdc.cols;
    let header = tdc.header;
    let newHeader = [];
    for(let h of header) {
      newHeader.push(h);
      newHeader.push('Type');
    }
    for(let i=0; i<cols[0].length; i++) {
      data[i] = [];
      for(let j=0; j<cols.length; j++) {
        data[i].push(cols[j][i].value);
        data[i].push(cols[j][i].type);
      }
    }
    
    return new HandsontableWrapper(
      data,
      newHeader
    );
  },
  validfile() {
    const ident = Router.current().params._id;
    const validFile = isValidFile(ident, 'preProcessing');
    const isTodo = processStatus(ident, 'preProcessing') == 'todo';
    if (!validFile && isTodo) {
      Router.go('startingTable', { _id: ident });
      Session.set('error', !Session.get('isResetted'));
    }
    return validFile;
  },
  processStatus() {
    const ident = Router.current().params._id;
    return processStatus(ident, 'preProcessing');
  },
});
