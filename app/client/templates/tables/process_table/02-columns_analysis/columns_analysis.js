import Handsontable from 'handsontable';
import {
  HandsontableWrapper,
  isValidFile,
  processStatus,
  buildColHeader,
} from '../../../../utils/utils';


Template.ColumnsAnalysis.helpers({
  handsontablewrapper_notprocessed() {
    const jsonFileCurrent = this.jsonFile;
    return new HandsontableWrapper(
      JSON.parse(jsonFileCurrent.data),
      Object.keys(JSON.parse(jsonFileCurrent.data)[0]),
    );
  },
  handsontablewrapper_processed() {
    const info = this.infoTable;
    const tableDatas = this.datas;
    const tableDataCopy = this.tableDataCopy;
    const jsonFileCurrent = this.jsonFile;
    const objectData = JSON.parse(jsonFileCurrent.data);

    const neCols = [];
    const liCols = [];

    // get NE col
    if (info.neCols !== undefined) {
      for (key of info.neCols) {
        neCols.push(key.index);
      }
    }

    // get literal col
    if (info.litCols !== undefined) {
      for (key of info.litCols) {
        liCols.push(key.index);
      }
    }
  
    const SubCols = info.subCol;
    

    const hlabel = Object.keys(objectData[0]);
    const headerNames = [];
    for (const index in hlabel) {
      const parseIntIndex = parseInt(index);

      headerNames.push(
        buildColHeader(parseIntIndex, ['all'], objectData),
      );
    }

    const columnsFunc = function (c) {
      const columnMeta = {};
      columnMeta.data = hlabel[c];
      return columnMeta;
    };

    function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
      // console.log(col);
      Handsontable.renderers.TextRenderer.apply(this, arguments);
      if (col === SubCols) {
        // td.style.fontWeight = 'bold';
        td.style.color = '#67429a';
        td.style.background = 'rgba(103, 66, 154, 0.08)';
      } else if (neCols.includes(col)) {
        // td.style.fontWeight = 'bold';
        td.style.color = '#20799f';
        td.style.background = 'rgba(32, 121, 159,.08)';
      } else if (liCols.includes(col)) {
        // td.style.fontWeight = 'bold';
        td.style.color = '#577b16';
        td.style.background = 'rgba(87, 123, 22, .08)';
      } else {
        td.style.color = '#63666c';
        td.style.background = 'rgba(99, 102, 108, .15)';
      }
    }

    const colorColumns = function () {
      const cellProperties = {};
      cellProperties.renderer = firstRowRenderer; // uses function directly
      return cellProperties;
    };

    const cellComment = [];
    const neColsIndex = [];
    this.infoTable.neCols.forEach(function (e) {
      neColsIndex.push(e.index);
    });


    // comment for data normalization
    const cols = tableDataCopy.cols;
    for (var i = 0; i < cols.length; i++) {
      if (neColsIndex.includes(i)) {
        cols[i].forEach(function (data, index) {
          cellComment.push({
            row: index,
            col: i,
            comment: {
              value: `${'<em>This cell has been normalized.</em> <br /> '
              + '<strong>Old value:</strong> '}${data.value_old
              }<span class="readMore"> Click the cell to read more...</span>`,
              readOnly: true,
            },
          });
        });
      }
    }
    return new HandsontableWrapper(objectData, headerNames, columnsFunc, colorColumns, cellComment);
  },
  validfile() {
    const ident = Router.current().params._id;
    const validFile = isValidFile(ident, 'columnsAnalysis');
    const isTodo = processStatus(ident, 'columnsAnalysis') == 'todo';

    if (!validFile && isTodo) {
      Router.go('preProcessing', { _id: ident });
      Session.set('error', !Session.get('isResetted'));
    }

    return validFile;
  },
  processStatus() {
    const ident = Router.current().params._id;
    return processStatus(ident, 'columnsAnalysis');
  },
});

Template.ColumnsAnalysis.onDestroyed(function () {
  Handsontable.hooks.remove('afterOnCellMouseDown');
});
