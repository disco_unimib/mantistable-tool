import {
  HandsontableWrapper,
  isValidFile,
  processStatus,
  buildColHeader, openRightSideBar, reRenderHandsontable,
} from '../../../../utils/utils';

Template.EditAnnotations.helpers({
  handsontablewrapper_processed() {
    const tableDatas = this.datas;

    const objectDataOriginal = tableDatas.map(function (x) {
      return x.stringDocOriginal;
    });

    const hlabel = Object.keys(objectDataOriginal[0]);
    const headerNames = [];
    for (const index in hlabel) {
      const parseIntIndex = parseInt(index);

      headerNames.push(
        buildColHeader(parseIntIndex),
      );
    }
    return new HandsontableWrapper(objectDataOriginal, headerNames, null, null);
  },
  validfile() {
    const ident = this._id;
    const validFile = isValidFile(ident, 'entityLinking');
    const isTodo = processStatus(ident, 'entityLinking') === 'todo';

    if (!validFile && isTodo) {
      Router.go('relationshipsAnnotation', { _id: ident });
      Session.set('error', !Session.get('isResetted'));
    }
    return validFile;
  },
});

Template.EditAnnotations.onRendered(function () {
  $('#infoBox .close').hide();
  openRightSideBar();
  reRenderHandsontable();
});
