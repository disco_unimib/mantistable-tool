import Handsontable from 'handsontable';
import {
  HandsontableWrapper,
  isValidFile,
  processStatus,
  buildColHeader,
} from '../../../../utils/utils';

Template.RelationshipsAnnotation.helpers({
  handsontablewrapper_notprocessed() {
    const jsonFileCurrent = this.jsonFile;
    return new HandsontableWrapper(
      JSON.parse(jsonFileCurrent.data),
      Object.keys(JSON.parse(jsonFileCurrent.data)[0]),
    );
  },
  handsontablewrapper_processed() {
    const info = this.infoTable;
    const tableDatas = this.datas;
    const jsonFileCurrent = this.jsonFile;
    const objectData = JSON.parse(jsonFileCurrent.data);
    const SubCols = info.subCol;
    const neCols = [];
    const liCols = [];
    const noAnnCols = [];

    // get NE col
    if (info.neCols !== undefined) {
      for (key of info.neCols) {
        neCols.push(key.index);
      }
    }

    // get literal col
    if (info.litCols !== undefined) {
      for (key of info.litCols) {
        liCols.push(key.index);
      }
    }

    // get no annotation col
    if (info.noAnnCols !== undefined) {
      for (key of info.noAnnCols) {
        noAnnCols.push(key.index);
      }
    }
   
    const hlabel = Object.keys(objectData[0]);
    const headerNames = [];
    for (const index in hlabel) {
      const parseIntIndex = parseInt(index);

      headerNames.push(
        buildColHeader(parseIntIndex, ['ne', 'lit'], objectData),
      );
    }

    const columnsFunc = function (c) {
      const columnMeta = {};
      columnMeta.data = hlabel[c];
      return columnMeta;
    };

    function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
      Handsontable.renderers.TextRenderer.apply(this, arguments);
      td.style.fontWeight = 'bold';
      td.style.color = 'green';
      td.style.background = '#CEC';
    }

    return new HandsontableWrapper(objectData, headerNames, columnsFunc, null);
  },
  validfile() {
    const ident = Router.current().params._id;
    const validFile = isValidFile(ident, 'relationshipsAnnotation');
    const isTodo = processStatus(ident, 'relationshipsAnnotation') == 'todo';

    if (!validFile && isTodo) {
      Router.go('conceptDatatypeAnnotation', { _id: ident });
      Session.set('error', !Session.get('isResetted'));
    }

    return validFile;
  },
  processStatus() {
    const ident = Router.current().params._id;
    return processStatus(ident, 'relationshipsAnnotation');
  },
});
