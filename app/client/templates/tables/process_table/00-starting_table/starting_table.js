import { closeRightSideBar, HandsontableWrapper } from '../../../../utils/utils';


/** ************************************************************************** */
/* StartingTable: Helpers */
/** ************************************************************************** */
Template.StartingTable.helpers({
  initialData() {
    const jsonFileCurrent = this.jsonFile;
    return new HandsontableWrapper(
      JSON.parse(jsonFileCurrent.data),
      Object.keys(JSON.parse(jsonFileCurrent.data)[0]),
    );
  },
});
