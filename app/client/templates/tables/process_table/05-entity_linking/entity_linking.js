import {
  HandsontableWrapper,
  isValidFile,
  processStatus,
  buildColHeader,
} from '../../../../utils/utils';

Template.EntityLinking.helpers({
  handsontablewrapper_notprocessed() {
    const jsonFileCurrent = this.jsonFile;
    return new HandsontableWrapper(
      JSON.parse(jsonFileCurrent.data),
      Object.keys(JSON.parse(jsonFileCurrent.data)[0]),
    );
  },
  handsontablewrapper_processed() {
    const info = this.infoTable;
    const tableDatas = this.datas;
    const tableDataCopy = this.tableDataCopy;
    const jsonFileCurrent = this.jsonFile;
    const objectData = JSON.parse(jsonFileCurrent.data);
  
    let keys = Object.keys(objectData[0]);
    for(let i=0; i<objectData.length; i++) {
      for(let j=0; j<keys.length; j++) {
        const entityLinkg = tableDataCopy.cols[j][i].linked_entity;
        if(entityLinkg && entityLinkg != '')
          objectData[i][keys[j]] += `<a href='${entityLinkg}' target="_blank">${entityLinkg}</a>`;
      }
    }

    const neCols = [];
    const liCols = [];
    const noAnnCols = [];

    // get NE col
    if (info.neCols !== undefined) {
      for (key of info.neCols) {
        neCols.push(key.index);
      }
    }

    // get literal col
    if (info.litCols !== undefined) {
      for (key of info.litCols) {
        liCols.push(key.index);
      }
    }

    // get no annotation col
    if (info.noAnnCols !== undefined) {
      for (key of info.noAnnCols) {
        noAnnCols.push(key.index);
      }
    }

    const hlabel = Object.keys(objectData[0]);
    const headerNames = [];
    for (const index in hlabel) {
      const parseIntIndex = parseInt(index);

      headerNames.push(
        buildColHeader(parseIntIndex, ['ne', 'lit'], objectData),
      );
    }

    const columnsFunc = function (c) {
      const columnMeta = {};
      columnMeta.data = hlabel[c];
      columnMeta.renderer = 'html';

      return columnMeta;
    };

    return new HandsontableWrapper(objectData, headerNames, columnsFunc, null);
  },
  validfile() {
    const ident = Router.current().params._id;
    const validFile = isValidFile(ident, 'entityLinking');
    const isTodo = processStatus(ident, 'entityLinking') == 'todo';

    if (!validFile && isTodo) {
      Router.go('relationshipsAnnotation', { _id: ident });
      Session.set('error', !Session.get('isResetted'));
    }

    return validFile;
  },
  processStatus() {
    const ident = Router.current().params._id;
    return processStatus(ident, 'entityLinking');
  },
});
