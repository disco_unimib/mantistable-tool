import moment from 'moment';
import { hideConsole } from '../../../utils/utils';

// whenever #showMoreResults becomes visible, retrieve more results
function showMoreVisible() {
  let threshold;

  const target = $('#showMoreResults');
  if (!target.length) return;

  threshold = $(window).scrollTop() + $(window).height() - target.height();

  if (target.offset().top < threshold) {
    if (!target.data('visible')) {
      // console.log('target became visible (inside viewable area)');
      target.data('visible', true);
      Session.set('limit',
        Session.get('limit') + 100);
    }
  } else if (target.data('visible')) {
    // console.log('target became invisible (below viewable arae)');
    target.data('visible', false);
  }
}

function getSearchQuery() {
  let search = Session.get('search');
  if (search === undefined) {
    search = '';
  }

  return search;
}

function getFilterQuery() {
  const filter = Session.get('filter');
  let queryFilter;

  if (filter === '' || filter === 'all' || filter === undefined) {
    queryFilter = { $ne: 0 };
  } else {
    queryFilter = filter;
  }

  return queryFilter;
}

function getGSQuery() {
  const gsSelected = Session.get('gsSelected');
  let queryGsSelected;

  if (gsSelected === '' || gsSelected === undefined) {
    queryGsSelected = { $ne: 0 };
  } else {
    queryGsSelected = gsSelected;
  }

  return queryGsSelected;
}

// list tables
Template.listTables.onRendered(() => {
  hideConsole();
});

// load tables
Template.loadTables.helpers({
  tables() {
    const search = getSearchQuery();
    const filter = getFilterQuery();
    const goldStandard = getGSQuery();
    return Tables.find({
      name: { $regex: search, $options: 'i' },
      globalStatus: filter,
      typeGS: goldStandard,
    }, { limit: Session.get('limit') });
  },
  moreResults() {
    const search = getSearchQuery();
    const filter = getFilterQuery();
    const goldStandard = getGSQuery();

    return !(Tables.find({
      name: { $regex: search, $options: 'i' },
      globalStatus: filter,
      typeGS: goldStandard,
    }).count() < Session.get('limit'));
  },
  noResults() {
    const search = getSearchQuery();
    const filter = getFilterQuery();
    const goldStandard = getGSQuery();

    return (Tables.find({
      name: { $regex: search, $options: 'i' },
      globalStatus: filter,
      typeGS: goldStandard,
    }).count() === 0);
  },
  hasDoingProcess() {
    const hasDoingProcess = InfoTable.find({
      _id: this._id,
      'statusProcess.status': 'doing',
    }).count() > 0;
    return hasDoingProcess ? 'processing' : null;
  },
  statusProcess() {
    let statusProcessCurrentTable = InfoTable.find({ _id: this._id });

    statusProcessCurrentTable = statusProcessCurrentTable.map(function (x) {
      return x.statusProcess;
    });
    return statusProcessCurrentTable[0];
  },
  status() {
    switch (this.status) {
      case 'todo':
      default:
        return 'times';
      case 'doing':
        return 'refresh fa-spin';
      case 'done':
        return 'check';
    }
  },
  time() {
    let start;
    let end;
    let total = 0;
    let totalDuration = '-';

    const infoTable = InfoTable.findOne({ _id: this._id });
    if (infoTable !== undefined) {
      const statusProcessCurrentTable = infoTable.statusProcess;

      for (const index in statusProcessCurrentTable) {
        start = statusProcessCurrentTable[index].start;
        end = statusProcessCurrentTable[index].end;

        total = moment(total).add(moment.duration((end - start)));
      }
      if (moment(total).isValid() && statusProcessCurrentTable !== undefined) {
        const seconds = total.seconds();
        const minutes = total.minutes();
        totalDuration = moment()
          .minutes(minutes)
          .seconds(seconds)
          .format('mm:ss');
      }
    }
    return totalDuration;
  },
  typeGS() {
    return Array.isArray(this.typeGS) ? this.typeGS : [this.typeGS];
  },

});

Template.loadTables.events({
  // processAll
  'click #processAll'(event) {
    event.stopPropagation();
    event.preventDefault();

    Meteor.apply('method1', [], { noRetry: true });

    setTimeout(function () {
      Meteor.apply('method2', [], { noRetry: true });
    }, 1000);

    setTimeout(function () {
      Meteor.apply('method3', [], { noRetry: true });
    }, 2000);

    setTimeout(function () {
      Meteor.apply('method4', [], { noRetry: true });
    }, 3000);

    //Meteor.apply('startAnnot', [], { noRetry: true });
  },

  // search
  'keyup input#search': _.debounce(function () {
    const searchVal = $('input#search').val();

    Session.set('search', searchVal);
    if (searchVal !== '') {
      $('.btn-cancel').show();
    } else {
      $('.btn-cancel').hide();
    }
  }, 100),

  'click .search .btn-cancel'() {
    $('input#search').val('');
    $('.btn-cancel').hide();
    Session.set('search', undefined);
  },

  // select
  'change select#goldStandard'() {
    const gsSelected = $('select#goldStandard').val();

    Session.set('gsSelected', gsSelected);
  },
});

Template.loadTables.onCreated(() => {
  Session.set('search', undefined);
  Session.set('filter', undefined);
  Session.set('gsSelected', undefined);
  Session.set('limit', 50);
});

Template.loadTables.onRendered(() => {
  Session.set('templateLoaded', true);
  $('.wrapper').css('height', 'auto');
  $('.wrapper').css('min-height', '100%');

  hideConsole();

  // limit tables load
  setTimeout(function () {
    showMoreVisible();
  }, 200);

  $('.table-responsive').on('load scroll', function () {
    showMoreVisible();
  });
});


// suggestion
Template.loadTables.onDestroyed(() => {
  $('.sticky-footer').removeClass('no-console');
  $('.content').removeClass('noConsole');
  Session.set('templateLoaded', false);
  Session.set('limit', undefined);
  Session.set('search', undefined);
  Session.set('filter', undefined);
  Session.set('gsSelected', undefined);
});

Template.startedSuggestion.onRendered(() => {
  Session.set('templateLoaded', true);
});

// filter
Template.filter.events({
  'click .filter .btn'(event) {
    if ($(event.currentTarget).hasClass('active')) {
      $('.filter .btn').removeClass('active');
      Session.set('filter', undefined);
    } else {
      $('.filter .btn').removeClass('active');
      const filter = $(event.currentTarget)
        .attr('class')
        .replace('btn', '')
        .trim();
      Session.set('filter', filter);
      $(event.currentTarget).addClass('active');
      Session.set('limit', 10);
    }
  },
});

Template.alertDelete.events({
  // deleteAll
  'click #deleteAll'(event) {
    Meteor.call('deleteAllTables');
  },
});
