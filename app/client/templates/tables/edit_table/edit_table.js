import { HandsontableWrapper, openRightSideBar, reRenderHandsontable } from '../../../utils/utils';

Template.EditTable.events({
  'click #delete'(e, t) {
    e.stopPropagation();
    e.preventDefault();
    const ident = Router.current().params._id;
    console.log(ident);
    Meteor.call('file-delete', ident,
      function (error, result) {
        if (result) {
          // Prevent routing before modal is dismissed
          $('#modal-danger')
            .on('hidden.bs.modal', function () {
              Router.go('tablesList');
            })
            .modal('hide');
        }
      });
  },
});

Template.EditTable.helpers({
  handsontablewrapper() {
    const jsonFileCurrent = Jsonfile.findOne({ _id: Router.current().params._id });
    return new HandsontableWrapper(
      JSON.parse(jsonFileCurrent.data),
      Object.keys(JSON.parse(jsonFileCurrent.data)[0]),
    );
  },
  uploadingTable() {
    return Session.get('uploadingTable');
  },
});

Template.EditTable.onCreated(function () {
});

Template.EditTable.onRendered(function () {
  Session.set('unvalidfile', false);
  Session.set('currentPageHeader', 'Edit Table');
  Session.set('editing', false);
  Session.set('uploadingTable', false);

  // sidebar right setup
  $('#infoBox .close').hide();
  openRightSideBar();
  reRenderHandsontable();
});

