Template.process.events({
  // processAll
  'click #processAll'(event) {
    event.stopPropagation();
    event.preventDefault();

    Meteor.apply('method1', [], { noRetry: true });

    setTimeout(function () {
      Meteor.apply('method2', [], { noRetry: true });
    }, 1000);

    setTimeout(function () {
      Meteor.apply('method3', [], { noRetry: true });
    }, 2000);

    setTimeout(function () {
      Meteor.apply('method4', [], { noRetry: true });
    }, 3000);

    //Meteor.apply('startAnnot', [], { noRetry: true });
  },
});