import moment from 'moment';
import {
  closeRightSideBar, getScrollBarWidth, openConsole, processIsCurrentlyRunning, reRenderHandsontable, runProcess,
} from '../../utils/utils';

Template.masterLayout.helpers({
  logs() {
    // tbd - change Alerts collection "state" to match route
    let routeName = Router.current().route.getName();

    switch (routeName) {
      case 'preProcessing':
        routeName = 'preprocessing';
        break;
      case 'conceptDatatypeAnnotation':
        routeName = 'candidateSelection';
        break;
      case 'relationshipsAnnotation':
        routeName = 'relationshipsAnnotations';
        break;
      case 'createTables':
        routeName = 'uploadTable';
        break;
    }

    return Alerts.find({ tableID: this._id, state: routeName });
  },
  createdAt() {
    return new Date();
  },
  tablesInfo() {
    const tablesInfo = [];

    tablesInfo.tablesTot = Tables.find({}).count();
    tablesInfo.tablesDone = Tables.find({ globalStatus: 'done' }).count();
    tablesInfo.tablesDoing = Tables.find({ globalStatus: 'doing' }).count();

    return tablesInfo;
  },
  currentTable() {
    return InfoTable.findOne({ _id: this._id });
  },
  tableName() {
    const table = Tables.findOne({ _id: this._id });
    if (table) {
      return table.name;
    }
  },
  timeToProcess() {
    let start;
    let end;
    let totalDuration = '-';

    const statusProcess = this.statusProcess;
    for (const index in statusProcess) {
      if (statusProcess[index].routeName === Router.current().route.getName()) {
        start = statusProcess[index].start;
        end = statusProcess[index].end;
      }
    }

    if (end - start > 0) {
      const duration = moment.duration((end - start));

      if (duration < 1000) {
        totalDuration = '< 1s';
      } else {
        const seconds = duration.seconds();
        const minutes = duration.minutes();
        totalDuration = moment().minutes(minutes).seconds(seconds).format('mm:ss');
      }
    }
    return totalDuration;
  },
  typeGS() {
    return Array.isArray(this.typeGS) ? this.typeGS : [this.typeGS];
  },
  disabledReset() {
    if (processIsCurrentlyRunning(this._id)) {
      return true;
    }
    return false;
  },
  error() {
    // reset session for error after message fadeout
    setTimeout(function () {
      $('#error').fadeOut(3000, function () {
        Session.set('error', undefined);
      });
    }, 3000);

    return Session.get('error');
  },
  templateIsLoaded() {
    return Session.get('templateLoaded');
  },
  cronMessageIsDismissed() {
    return sessionStorage.getItem('cronMessageIsDismissed');
  },
});

Template.masterLayout.onCreated(() => {
  $('body').addClass('sidebar-mini sidebar-collapse');
  Session.set('templateLoaded', false);
});

Template.masterLayout.onRendered(() => {
  // rerender handsontable when window change size
  $(window).resize(function () {
    reRenderHandsontable();
  });
});

Template.masterLayout.events({
  'click #download_ann'(event) {
    event.stopPropagation();
    event.preventDefault();

    // call a server route that serves a dinamically generated JSON
    window.open('/downloadMyAnnotations', '_blank');
  },

  'click #download_preview'(event) {
    event.stopPropagation();
    event.preventDefault();

    window.open('/downloadPreview', '_blank');
    $('#previewModal').modal('hide');
  },

  'click #CTAround1'(event) {
    event.stopPropagation();
    event.preventDefault();
    window.open('/downloadForChallenge/CTA', '_blank');
  },

  'click #CPAround1'(event) {
    event.stopPropagation();
    event.preventDefault();
    window.open('/downloadForChallenge/CPA', '_blank');
  },

  'click #CEAround1'(event) {
    event.stopPropagation();
    event.preventDefault();
    window.open('/downloadForChallenge/CEA', '_blank');
  },

  // expand/reduced left sidebar
  'click .sidebar-toggle'() {
    $('body').toggleClass('sidebar-collapse');
    reRenderHandsontable();
  },

  // close right sidebar
  'click .control-sidebar .close'() {
    closeRightSideBar();
  },

  // open/close console
  'click #toggle-console'() {
    $('.sticky-footer').toggleClass('close-console');
    $('.sticky-footer').toggleClass('open-console');
    $('.up-down-arrow').toggleClass('fa-caret-up');
    $('.up-down-arrow').toggleClass('fa-caret-down');

    $('.multiStep').css('margin-bottom', $('.sticky-footer').hasClass('open-console') ? '150px' : '');
    reRenderHandsontable();
  },

  // reset all process
  'click .reset'(e) {
    e.preventDefault();
    Meteor.call('setInitialInfoTable', this._id);
    Meteor.call('updateGlobalStatus', this._id, 'todo');
    Session.set('isResetted', true);

    setInterval(function () {
      Session.set('isResetted', false);
    }, 200);
  },

  // run again current page process
  'click .rerun'(e) {
    e.preventDefault();

    const ident = Template.instance().data._id;
    const process = Router.current().route.getName();
    closeRightSideBar();

    Meteor.call('updateGlobalStatus', ident, 'doing');
    Meteor.call('updateProcessStatus', ident, process, 'doing');

    Meteor.call(process, ident, function (error) {
      if (!error) {
        // find if there are some step not done yet
        const hasTodoProcess = InfoTable.find({
          _id: ident,
          'statusProcess.status': 'todo',
        }).count() > 0;

        if (!hasTodoProcess) {
          Meteor.call('updateGlobalStatus', ident, 'done');
        } else {
          Meteor.call('updateGlobalStatus', ident, 'doing');
        }
      }
    });

    openConsole();

    Session.set('isResetted', true);

    setInterval(function () {
      Session.set('isResetted', false);
    }, 200);
  },

  // set session for cron message dismiss
  'click .cron .btn'() {
    sessionStorage.setItem('cronMessageIsDismissed', true);

  },
});
