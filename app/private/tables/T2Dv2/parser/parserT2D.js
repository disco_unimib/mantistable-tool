var args = process.argv.slice(2);
var fs = require('fs');
try {
	var files = fs.readdirSync(args[0]);
}
catch(e) {
	console.log("Missing or Wrong arguments")
	return;
}


for(var file of files) {
	var lines = fs.readFileSync('tables/'+file).toString();
	lines = JSON.parse(lines);
	var columns = lines.relation;
	var table = parseTable(columns);
	var dir = 'myConverted';
	
	if (!fs.existsSync(dir))
		fs.mkdirSync(dir)
	
	fs.writeFileSync(dir+'/'+file, JSON.stringify(table, null, 4));
}

function parseTable(columns) {
	var header = [];
	var nh = 0; // no header number

	for(var i=0; i<columns.length; i++) {
		if(columns[i][0] == '' || columns[i][0] == '?')
			header.push('NO-HEADER'+(++nh));
		else
			header.push(columns[i][0]);				
	}
	
	var table = [];
	var numRows = columns[0].length;
	for(var i=1; i<numRows; i++) {
		var row = {};
		for(var j=0; j<header.length; j++) {
			row[header[j]] = replacer(columns[j][i]);
		}
		table.push(row);			
	}
	return table;
}

function replacer(value) {
	return value.replace(/[^\w\s]/gi, '');
}