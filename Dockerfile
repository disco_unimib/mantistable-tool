FROM node:8-alpine

ENV APP_BUNDLE_FOLDER /opt/bundle
ENV SCRIPTS_FOLDER /docker
COPY ./app/app.tar.gz /opt/app.tar.gz
# Install OS build dependencies, which we remove later after we’ve compiled native Node extensions
RUN apk --no-cache --virtual .node-gyp-compilation-dependencies add \
		g++ \
		make \
		python \
		tar \
	# And runtime dependencies, which we keep
	&& apk --no-cache add \
		bash \
		ca-certificates

RUN cd /opt && tar -zxvf app.tar.gz
RUN cd /opt/bundle/programs/server && npm install

WORKDIR $APP_BUNDLE_FOLDER
RUN exec "$@" 
CMD ["node", "main.js"]