# Citing

[1] Cremaschi, M., De Paoli, F., Rula, A., & Spahiu, B. (2020). A fully automated approach to a complete semantic table interpretation. Future Generation Computer Systems, 112, 478-500.

[2] Cremaschi, M., Avogadro, R., & Chieregato, D. (2019). MantisTable: an Automatic Approach for the Semantic Table Interpretation. SemTab@ ISWC, 2019, 15-24.

[3] Cremaschi, M., Rula, A., Siano, A., & De Paoli, F. (2019). MantisTable: a tool for creating semantic annotations on tabular data. 
In The Semantic Web: ESWC 2019 Satellite Events: ESWC 2019 Satellite Events, Portorož, Slovenia, June 2–6, 2019, Revised Selected Papers 16 (pp. 18-23). Springer International Publishing.

[4] Cremaschi, M., Rula, A., Siano, A., & De Paoli, F. (2019). Semantic Table Interpretation Using MantisTable. In OM@ ISWC (pp. 195-196).

# Warning deprecated

Checkout [https://bitbucket.org/disco_unimib/mantistable-tool-3](https://bitbucket.org/disco_unimib/mantistable-tool-3)

# MantisTable tool

MantisTable is a tool that implements the Semantic Table Interpretation workflow to annotate tables.
It is a web application allowing its users to execute the approach described in:

Cremaschi, M and De Paoli, F. and Rula, A. and Spahiu, B. (2019). _A fully automated approach to a complete Semantic Table Interpretation_. 
Manuscript submitted for publication.


**Keywords**: table, entity linking, table annotation, table interpretation, semantic table interpretation, semantic web, relation extraction.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Download the Node.js installer for your platform at [Nodejs.org](https://nodejs.org/en/download/)
2. Download the Meteor installer at [Meteor.com](https://www.meteor.com/install)
3. Install the [Iron Command Line tool](https://github.com/iron-meteor/iron-cli) globally so you can use it from any project directory

```
$ git clone https://github.com/iron-meteor/iron-cli.git
$ cd iron-cli
$ nano package.json
```

Edit the version of the fibers package with an * instead of "fibers": "1.0.15" -> "fibers": "*" because it's broken in the last version of node.js

```
$ npm install -g
```
And install it globally.

## Running

From the root directory of the project, launch the following command:

```
$ iron run
```

Open the web browser and go to <http://localhost:3000/> for testing the tool.


## Docker

```
docker-compose up
```

## License

Apache 2.0
